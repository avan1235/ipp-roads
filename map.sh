#!/bin/bash

# Task constants definition
readonly MIN_ROUTE_NUM=1
readonly MAX_ROUTE_NUM=999

# Script constants definition
readonly EXIT_STRING="Leaving testing script..."
# Can be set to true to display messages on exit
readonly PRINT_MESSAGES_ON_FAIL_EXIT=false

# Define a function that exists the script and can print some message
function exit_fail ()
{
    if ${PRINT_MESSAGES_ON_FAIL_EXIT}
    then
        messages=( "$@" )
        for msg in "${messages[@]}"
        do
            echo -e "$msg"
        done
        echo -e "$EXIT_STRING"
    fi
    exit 1
}

# Exit when there are less than 2 parameters given to script
if [[ $# -lt 2 ]]
then
    exit_fail "Usage: ./map.sh [input_file] [route_number_1] [...]"
fi

file="$1"

# Exist when the first parameter is not an existing file path
if [[ ! -f "$file" ]]
then
    exit_fail "Input file $file does not exist"
fi

ids=( "$@" )
# Remove first param from array which is input file name
unset "ids[0]"

error_messages=()
has_error=false

for id in "${ids[@]}"
do
    # Regex match unsigned number only
    if [[ ! "$id" =~ ^[0-9]+$ ]]
    then
        error_messages+=("$id is not a valid unsigned number")
        has_error=true
    # Check route number range for number in ids
    elif [[ "$id" -lt "$MIN_ROUTE_NUM" || "$id" -gt "$MAX_ROUTE_NUM" ]]
    then
        error_messages+=("$id is not a valid route number")
        has_error=true
    else
        # Calculate length by grepping the line with specified route id,
        # getting only the first line if many grepped (should be only one)
        # changing all ';' to newline symbols '\n' and getting only
        # every third line which consist the length of the road in route.
        # Then change all new line symbols between the lines into '+' and
        # execute calculator bc command on such output
        len=$( grep -E "^${id};" "$file"  | head -1 | tr ';' '\n' | awk 'NR % 3 == 0' | paste -sd+ | bc )

        # Check if len variable is not empty or empty string
        # and if not then show the result to standard output
        if [[ ! -z "$len" ]];  then echo "$id;$len"; fi
    fi
done

if ${has_error}
then
    exit_fail "${error_messages[@]}"
fi

exit 0