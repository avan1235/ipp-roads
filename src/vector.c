/** @file
 * Implementation of the class keeping the vector data structure
 * of pointers in the dynamic array of pointers.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "vector.h"

/**
 * @brief Resize vector data array.
 * Change the vector's array of data size and reallocate all
 * data save in vector in new memory. When encounter memory allocation
 * problem returns false and does not change the vector.
 * @param[in] vector        – vector to be changed
 * @return                   – true when successfully changed size
 *                             of vector otherwise false
 */
static bool resizeVector(vector_t *vector)
{
    unsigned newVectorSize = sizeof(void *) * vector->memory_size * LINE_GROWTH_FACTOR;
    void **tempPtr = (void **) mem_realloc(vector->data, newVectorSize);

    if (tempPtr == NULL)
        return false;

    vector->data = tempPtr;
    vector->memory_size *= LINE_GROWTH_FACTOR;

    return true;
}

/**
 * @brief Check vector range index.
 * Change if vector index is not out of range of the current vector size.
 * @param[in] vector        – vector to be checked
 * @param[in] index         – index to be checked
 * @return                  – true when problem with index otherwise false
 */
static bool wrongVectorRange(vector_t *vector, unsigned index)
{
    return index >= vector->size;
}

/**
 * @brief Moves vector data to left.
 * Shifts all vector data to left after for example removing some
 * vector index.
 * @param[in] vector            – vector which elements are moved
 * @param[in] index             – index from which shifts starts
 * @param[in] movedElements     – number of elements in vector to move
 */
static void moveLeftData(vector_t *vector, unsigned index, unsigned movedElements)
{
    unsigned i = 0;
    while (i < movedElements) {
        vector->data[index + i] = vector->data[index + i + 1];
        i += 1;
    }
}

vector_t *initVector(void)
{
    vector_t *vector = mem_alloc(sizeof(vector_t));

    vector->data = (void **) mem_alloc(sizeof(void *) * INIT_VECTOR_MEMORY_SIZE);

    if (vector->data == NULL)
        return NULL;

    vector->memory_size = INIT_VECTOR_MEMORY_SIZE;
    vector->size = 0;
    return vector;
}

unsigned sizeOfVector(vector_t *vector)
{
    if (vector != NULL)
        return vector->size;

    return 0;
}

bool appendToVector(vector_t *vector, void *value)
{
    if (vector->size >= vector->memory_size && !resizeVector(vector))
        return false;

    vector->data[vector->size] = value;
    vector->size += 1;

    return true;
}

void* getFromVector(vector_t *vector, unsigned index)
{
    if (wrongVectorRange(vector, index))
        return NULL;

    return vector->data[index];
}

void* removeFromVector(vector_t *vector, unsigned index)
{
    if (wrongVectorRange(vector, index))
        return NULL;

    void *removedValue = vector->data[index];
    unsigned numberOfMoved = vector->size - index - 1;

    if (numberOfMoved > 0) {
        moveLeftData(vector, index, numberOfMoved);
    }
    vector->size -= 1;
    return removedValue;
}

void freeVectorContent(vector_t *vector, void (*freeNodeFun)(void *))
{
    if (vector != NULL) {
        for (unsigned i = 0; i < vector->size; ++i) {
            freeNodeFun((vector->data)[i]);
            (vector->data)[i] = NULL;
        }
        vector->size = 0;
    }
}

void freeVector(struct vector *vector)
{
    if (vector != NULL) {
        mem_free(vector->data);
        mem_free(vector);
    }
}