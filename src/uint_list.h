/** @file
 * Interface of the class keeping the single linked list
 * data structure of unsigned value in the list nodes.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_UINT_LIST_H
#define IPP_ROADS_UINT_LIST_H

#include <stdbool.h>
#include <stdlib.h>

#include "memory_manager.h"

/**
 * Type of unsigned list node structure.
 */
typedef struct uint_list_node uint_list_node_t;

/**
 * Pointer defined type to simplify the usage of
 * lists and call the list as 'list_t' not as the
 * node pointer 'list_node_t*'
 */
typedef struct uint_list_node* uint_list_t;

/**
 * Structure of the  list which is single linked list
 * keeping the unsigned value.
 */
struct uint_list_node {
    unsigned data;      ///< Value kept in list node
    uint_list_t next;   ///< Pointer to next list node
};

/**
 * @brief Insert new node to begin of list.
 * Insert value to given list by creating the new
 * head of the list and setting the pointer in the new
 * head to point to newly created structure outside
 * the inserting function.
 * When it is impossible to insert the new node to list
 * the old list is not changed and the false value is
 * returned by function.
 * @param[in, out] listPtr  – pointer to list to be edited
 * @param[in] value         – value to be inserted to list
 * @return                  – true when process of inserting
 *                            node was successful otherwise
 *                            false
 */
bool insertToUIntList(uint_list_t *listPtr, unsigned value);

/**
 * @brief Remove given value from list.
 * Remove from list the node which has a value equal
 * to deleteValue.
 * @param[in,out] listPtr    – list (head pointer) to be edited
 * @param[in] deleteValue    – value which has to be deleted from list
 */
void removeFromUIntList(uint_list_t *listPtr, unsigned deleteValue);

/**
 * @brief Free list nodes with data.
 * Free the list nodes and the values inside them.
 * @param[in] list        – list (head pointer) to be freed
 */
void freeUIntList(uint_list_t list);

#endif //IPP_ROADS_UINT_LIST_H
