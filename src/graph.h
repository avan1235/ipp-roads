/** @file
 * Interface of the class keeping the graph data structure
 * which is connected with some city tree.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_GRAPH_H
#define IPP_ROADS_GRAPH_H

#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <inttypes.h>

#include "vector.h"
#include "uint_list.h"
#include "list.h"
#include "min_heap.h"
#include "city_tree.h"

/**
 * Type of graph edge structure.
 */
typedef struct graph_edge graph_edge_t;

/**
 * Type of graph vertex structure.
 */
typedef struct graph_vertex graph_vertex_t;

/**
 * Type of graph structure.
 */
typedef struct graph graph_t;

/**
 * Structure to represent graph edge which has length
 * build year and a node which is adjacent to.
 */
struct graph_edge {
    unsigned adjacentTo;    ///< Number of adjacent vertex
    unsigned length;        ///< Length of edge
    int buildYear;          ///< Build year of edge
};

/**
 * Graph vertex structure which hold the length of the
 * name of city connected with node, pointer to city name
 * in city names tree, edges connected with node
 * and the list of routes to which the vertex belongs to.
 */
struct graph_vertex {
    unsigned cityNameLength; ///< Length of name of city connected with vertex
    tree_t endOfName;        ///< Pointer to end of connected city name in tree
    list_t edges;            ///< List of edges of vertex
};

/**
 * Graph represented by the vector of vertices and keeping
 * number of already inserted vertices to graph.
 */
struct graph {
    unsigned numberOfVertices;  ///< Number of vertices in graph
    vector_t *vertices;         ///< Vector of vertices in graph
};

/**
 * Init graph structure by creating all of its components.
 * @return          – pointer to new graph, when error in allocation
 *                    return NULL pointer
 */
graph_t* initGraph(void);

/**
 * @brief Add new vertex to graph.
 * Insert new vertex to graph by creating new vertex structure
 * and inserting it to graph vector of vertices.
 * @param[in] graph             – graph to insert new node in
 * @param[in] cityNameLength    – length of city name to be connected
 *                                with inserted vertex
 * @param[in] endOfName         – pointer to end of name of the city connected
 *                                with the inserted vertex
 * @return                      – true when succes otherwise false
 */
bool insertNewVertex(graph_t *graph, unsigned cityNameLength, tree_t endOfName);

/**
 * @brief Add new edge to graph.
 * Insert new edge to graph between two given vertices numbers. When vertices
 * does not exists in graph or allocation memory error does not change anything
 * in graph.
 * @param[in] graph         – graph to insert new edge
 * @param[in] a             – number of one of vertices of edge
 * @param[in] b             – number of second of vertices of edge
 * @param[in] length        – length of inserted edge
 * @param[in] buildYear     – build year of inserted edge to graph
 * @return                  – true when success otherwise false
 */
bool insertNewEdge(graph_t *graph, unsigned a, unsigned b, unsigned length, int buildYear);

/**
 * @brief Get length of city name.
 * Get the length of the city connected with the given vertex in graph
 * @param[in] vertex        – pointer to vertex in graph
 * @return                  – length of the name of city connected
 *                            with given vertex in graph
 */
unsigned cityNameLengthOfVertexPtr(graph_vertex_t *vertex);

/**
 * @brief Get number of vertices in graph.
 * Get the number of vertices in graph. For NULL pointer to graph returns 0.
 * @param[in] graph         – graph to check number of vertices
 * @return                  – number of vertices in graph
 */
unsigned numberOfVertices(graph_t *graph);

/**
 * @brief Get graph vertex pointer.
 * Get the pointer to specified graph vertex. When vertex of given index
 * does not exist in graph returns NULL pointer.
 * @param[in] graph         – graph to search for vertex
 * @param index             – index of vertex in graph
 * @return                  – pointer to vertex in graph
 */
graph_vertex_t* getGraphVertexPtr(graph_t *graph, unsigned index);

/**
 * @brief Find edge in graph.
 * Get the edge by which one vertex is adjacent to another in graph.
 * When vertices are not connected return false and foundEdgePtr is
 * changed to NULL pointer and return false.
 * @param[in] graph             – graph to search for edge
 * @param[in] from              – number of vertex to search for edge from
 * @param[in] to                – number of vertex to search for edge to
 * @param[in,out] foundEdgePtr  – pointer to found edge
 * @return                      – true when edge was found otherwise false
 */
bool isAdjacentTo(graph_t *graph, unsigned from, unsigned to, graph_edge_t **foundEdgePtr);

/**
 * @brief Check if edge exists in graph.
 * Check if there is edge in graph that connects given vertices
 * @param[in] graph             – graph to check for edge
 * @param[in] from              – begin vertex number of edge in graph
 * @param[in] to                – end vertex number of edge in graph
 * @return                      – true when edge exists otherwise false
 */
bool isAdjacentToCheck(graph_t *graph, unsigned from, unsigned to);

/**
 * @brief Get the age of graph edge.
 * Get the information about build year of edge in graph. When
 * edge not exists in graph returns 0.
 * @param[in] graph             – graph to check for edge
 * @param[in] from              – begin vertex number of edge in graph
 * @param[in] to                – end vertex number of edge in graph
 * @return                      – specified graph edge build year
 */
int getGraphEdgeYear(graph_t *graph, unsigned from, unsigned to);

/**
 * @brief Get number of vertex in graph.
 * Get The number of vertex based on its pointer in graph. When
 * vertex not exists in graph returns UINT_MAX.
 * @param[in] vertex            – pointer to vertex in graph
 * @return                      – number of graph vertex
 */
unsigned getGraphVertexNumber(graph_vertex_t *vertex);

/**
 * @brief Get pointer to specified edge.
 * Get the edge pointer in graph that is from one vertex number
 * to another given in argument. When there is no such edge in
 * graph returns NULL pointer.
 * @param[in] graph             – graph to search for edge
 * @param[in] from              – number of vertex where edge begin
 * @param[in] to                – number of vertex where edge ends
 * @return                      – pointer to the specified edge in graph
 */
graph_edge_t *getGraphEdgePtr(graph_t *graph, unsigned from, unsigned to);

/**
 * @brief Get the length of edge in graph.
 * For given edge pointer of graph get the length of this edge.
 * @param[in] edge              – pointer to edge to check the length of
 * @return                      – length of edge in graph
 */
unsigned getGraphEdgePtrLength(graph_edge_t *edge);

/**
 * @brief Get the year of edge in graph
 * When graph edge is NULL pointer returns the special year 0.
 * @param[in] edge              – pointer to edge to check the year of
 * @return                      – build/repair year of graph edge
 */
int getGraphEdgePtrYear(graph_edge_t *edge);

/**
 * @brief Find shortest path in graph between two vertices.
 * Find the shortest path in graph using Dijkstra algorithm
 * with min heap. Returns the vector of lists of pointers to cities (graph
 * vertices) on shortest paths.When path does not exists or there was a memory
 * allocation error returns NULL pointer instead of vector.
 * @param[in] graph                 – graph to search for the shortest path
 * @param[in] from                  – starting vertex in graph
 * @param[in] to                    – end vertex of path in graph
 * @param[in] canUseAllVertices     – when all vertices can be used in graph
 *                                    traversal true otherwise false
 * @param[in] canVertexBeUsed       – when canUseAllVertices is false this
 *                                    array is checked to determine if vertex
 *                                    can be used in traversal
 * @param[in] canUseAllEdges        – when all edges can be used in graph
 *                                    traversal then true otherwise false
 * @param[in] edgeB                 – when canUseAllEdges is set to false
 *                                    than the edge defined by these two
 *                                    vertices cannot be used in traversal
 * @param[in] edgeE                 – end of the not used edge
 * @return                          – array of shortest paths from the given
 *                                    source vertex. Array has to be freed by
 *                                    user
 */
vector_t *findShortestPathUsingNodesOmitEdge
        (graph_t *graph, unsigned from, unsigned to, bool canUseAllVertices, const bool *canVertexBeUsed,
                                                     bool canUseAllEdges, unsigned edgeB, unsigned edgeE);

/**
 * @brief Get the pointer to graph vertex that exists.
 * When user is sure that the city already exists in map, can get the pointer
 * to the graph vertex based only on the name of the city and the tree of cities
 * names.
 * @param[in] graph                 – pointer to graph to search for vertex
 * @param[in] city                  – city name string
 * @param[in] tree                  – tree of cities in graph
 * @return                          – pointer to existing node in graph. When
 *                                    node not exists returns NULL.
 */
graph_vertex_t *getExistingGraphVertexPtr(graph_t *graph, const char *city, tree_t tree);

/**
 * @brief Get the pointer to graph edge that exists.
 * When user is sure that the road already exists in map, can get the pointer
 * to the graph edge based only on pointers to adjacent vertices.
 * @param[in] graph                 – pointer to graph to search for edge
 * @param[in] from                  – pointer to vertex in which edge begin
 * @param[in] to                    – pointer to vertex in which edge ends
 * @return                          – pointer to existing edge in graph. When
 *                                    edge not exists returns NULL.
 */
graph_edge_t *getExistingGraphEdgePtr(graph_t *graph, graph_vertex_t *from, graph_vertex_t *to);

/**
 * @brief Get the number of graph vertex based on its city name
 * Get the number of vertex in graph when sure that this city exists in graph.
 * When the city name not exists in graph or cities tree the MAX_UINT value
 * is returned and should be treated as error code.
 * @param[in] graph                 – pointer to graph to search for vertex
 * @param[in] city                  – string with the name of the city
 * @param[in] tree                  – tree of cities to search for city name
 * @return                          – number of vertex of given city in graph
 */
unsigned getExistingGraphVertexNumber(graph_t *graph, const char *city, tree_t tree);

/**
 * @brief Free all memory allocated for graph structure.
 * Free the memory allocate for all vertices and edges in graph
 * @param[in] graph                 – graph to be freed
 */
void freeAllGraphData(graph_t *graph);

#endif //IPP_ROADS_GRAPH_H
