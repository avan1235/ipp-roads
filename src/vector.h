/** @file
 * Interface of the class keeping the vector data structure
 * of pointers in the dynamic array of pointers.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_VECTOR_H
#define IPP_ROADS_VECTOR_H

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>

#include "memory_manager.h"

/**
 * Size of the new, empty initialized vector
 */
#define INIT_VECTOR_MEMORY_SIZE 64

/**
 * Growth factor of vector
 */
#define LINE_GROWTH_FACTOR 2

/**
 * Vector type of vector structure.
 */
typedef struct vector vector_t;

/**
 * Vector of pointers structure to keep the data
 * in unified way. Memory has to be manually allocated y user
 * and te the pointer to allocated memory can be store in
 * vector. After freeing the vector memory has to be freed
 * manually.
 */
struct vector {
    void **data;            ///< Array of pointers in vector
    unsigned memory_size;   ///< Current capacity of vector
    unsigned size;          ///< Current number of pointers in vector
};

/**
 * @brief Init vector structure.
 * Initialize vector structure. Returns NULL pointer
 * when initializing vector was't possible.
 * @return      pointer to the initialized vector structure
 */
vector_t* initVector(void);

/**
 * @brief Get size of vector.
 * Get the current size of the vector so the number
 * of elements in it. For uninitialized vector returns 0.
 * @param[in] vector  – pointer to vector which size to determine
 * @return              size of vector
 */
unsigned sizeOfVector(vector_t *vector);

/**
 * @brief Add new value to vector.
 * Appends to the end of vector a new pointer to the allocated
 * value. Value has to be allocated outside the vector so the
 * user of vector has to remember about freeing this memory
 * after removing the pointer from the vector structure.
 * When there was memory allocation error when changing
 * size of vector to fit the size of data then the vector is
 * not changed and the memory allocated outside the vector
 * has to be freed outside the vector.
 * @param[in] vector  – pointer to structure to which data
 *                      will be appended
 * @param[in] value   – pointer to value to be appended
 * @return              true when process of appending value was
 *                      successful, otherwise returns false
 */
bool appendToVector(vector_t *vector, void *value);

/**
 * @brief Get value from vector.
 * Get the inserted pointer to vector at position index.
 * When there is no such element returns NULL pointer.
 * The returned pointer is a void* pointer so it should be
 * casted to special case of pointer.
 * @param[in] vector – pointer to structure from which data
 *                     will be read
 * @param[in] index  – index of the pointer in vector
 * @return             inserted pointer to vector at
 *                     position index
 */
void* getFromVector(vector_t *vector, unsigned index);

/**
 * @brief Remove value from vector.
 * Removes from vector the value at specified index and returns
 * the removed value as a result of of function. Shifts data in vector
 * to left after removing value from vector.
 * @param[in] vector    – vector to remove value from
 * @param[in] index     – index of value to be removed
 * @return              – removed value from vector. When bad index
 *                        given as parameter returns NULL pointer.
 */
void* removeFromVector(vector_t *vector, unsigned index);

/**
 * @brief Free the vector content
 * Remove the data to which the pointers in vector points to without freeing
 * the memory allocated for vector
 * @param[in] vector    – vector to be freed
 * @param[in] freeNodeFun – pointer to function
 *                          which can free pointed structures
 */
void freeVectorContent(vector_t *vector, void (*freeNodeFun)(void *));

/**
 * @brief Free the vector data structure
 * Remove all vector data and leave the data to which the pointers in vector
 * points to.
 * @param[in] vector    – vector to be freed
 */
void freeVector(vector_t *vector);

#endif //VECTOR_H
