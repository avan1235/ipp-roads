/** @file
 * Interface of the class reading the data from standard input.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_SCANNER_H
#define IPP_ROADS_SCANNER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#include "memory_manager.h"
#include "input_properties.h"

/**
 * Minimum line length for allocation line reading in
 * input scanner
 */
#define INIT_LINE_LEN 128

/**
 * Growth factor for memory of input line
 */
#define LINE_GROWTH_FACTOR 2

/**
 * @brief Reads line of command till END_OF_LINE or EOF
 * Reads characters from standard input and changes all characters from
 * out of range to JUNK_SYMBOL. When input line is empty line so has
 * only NEW_LINE_SYMBOL creates a string with this symbol. Otherwise the output
 * string does not contain the NEW_LINE_SYMBOL and ends with END_OF_STRING
 * symbol. When the command not end with NEW_LINE_SYMBOL it is changed
 * to contain the JUNK_SYMBOL to be treated by parser as the bad command.
 * @param[in,out] linePtr   – pointer to string to be changed as input
 * @return                  – length of the input string. Till length is greater
 *                            than 0 there are still some lines read by parser.
 *                            On memory allocation error returns 0 and the
 *                            last memory allocated for input line should be
 *                            freed by user outside this function
 */
uint64_t getInputLine(char **linePtr);

#endif //IPP_ROADS_SCANNER_H
