/** @file
 * Interface of the class executing the commands based on parser decisions.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_COMMANDS_MANAGER_H
#define IPP_ROADS_COMMANDS_MANAGER_H

#include <stdbool.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>

#include "memory_manager.h"
#include "map.h"
#include "input_command.h"

/**
 * @brief Execute the command got from parser
 * Get the command which can be empty and the parameters of parser working (can
 * get the information that the parsing process was successfully finished but
 * command should by treated as empty command so its value is not considered)
 * Do the specified by command tasks on map and set the out string to some value
 * if data has to be printed by printing function.
 * At finish frees the memory allocated for command arguments.
 * @param[in,out] map           – pointer to amp to be edited
 * @param[in] cmd               – command to be executed
 * @param[in] hasCommand        – when command was read then true otherwise false
 * @param[in] inputError        – when got bad input then true otherwise false
 * @param[in,out] outString     – pointer to string to be set after execution
 * @return                      – result of command execution or false when
 *                                encounter memory allocation error
 */
bool executeCommand(Map *map, command_t cmd, bool hasCommand, bool inputError, const char **outString);

/**
 * @brief Print error with specified number.
 * Print error message to standard error output using the information of the
 * current number of input command.
 * @param[in] lineNumber        – number of line of last command
 */
void printError(uint64_t lineNumber);

/**
 * @brief Print out data
 * When needed so the data pointer is not NULL the it is printed to standard
 * output and then it is freed by this function.
 * @param[in] data          – pointer to string to be printed and freed
 */
void manageOutData(const char *data);

#endif //IPP_ROADS_COMMANDS_MANAGER_H
