#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

#include "memory_manager.h"
#include "map.h"
#include "input_command.h"
#include "scanner.h"
#include "parser.h"
#include "commands_manager.h"

int main()
{
    char *inputLine = NULL;
    const char *outStr = NULL;
    uint64_t lineNumber = 1;
    command_t currentCommand;
    bool hasCommand, isInputError;

    Map *map = newMap();
    if (map == NULL)
        return EXIT_SUCCESS;

    while (getInputLine(&inputLine) >= MIN_INPUT_LINE_LEN)
    {
        if (!parseData(inputLine, &currentCommand, &hasCommand, &isInputError))
            printError(lineNumber);
        else if (!executeCommand(map, currentCommand, hasCommand, isInputError, &outStr))
            printError(lineNumber);
        else
            manageOutData(outStr);

        lineNumber += 1;
        mem_free(inputLine);
    }
    mem_free(inputLine);
    deleteMap(map);

    return EXIT_SUCCESS;
}

