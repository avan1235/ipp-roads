/** @file
 * Interface of the class keeping the single linked list
 * data structure of pointers in the list nodes.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_LIST_H
#define IPP_ROADS_LIST_H

#include <stdbool.h>
#include <stdlib.h>

#include "memory_manager.h"

/**
 * Type of list node structure
 */
typedef struct list_node list_node_t;

/**
 * Pointer defined type to simplify the usage of
 * lists and call the list as 'list_t' not as the
 * node pointer 'list_node_t*'
 */
typedef list_node_t* list_t;

/**
 * Structure of the  list which is single linked list
 * keeping the pointer to some data and a pointer
 * to the next node.
 */
struct list_node {
    void *data;     ///< Data pointer kept in list node
    list_t next;    ///< Pointer to next list node
};

/**
 * @brief Insert new node to begin of list.
 * Insert pointer to given list by creating the new
 * head of the list and setting the pointer in the new
 * head to point to newly created structure outside
 * the inserting function.
 * When it is impossible to insert the new node to list
 * the old list is not changed and the false value is
 * returned by function.
 * @param[in, out] listPtr  – pointer to list to be edited
 * @param[in] newData       – pointer to be inserted to list
 * @return                  – true when process of inserting
 *                            node was successful otherwise
 *                            false
 */
bool insertToList(list_t *listPtr, void *newData);

/**
 * @brief Remove given pointer from list.
 * Remove from list the node which has a pointer equal
 * to deleteData pointer and leave the data to which
 * the pointer in the node points to untouched
 * @param[in,out] listPtr    – list (head pointer) to be edited
 * @param[in] deleteData     – pointer to data which has
 *                             to be deleted from list
 */
void removeFromList(list_t *listPtr, void *deleteData);

/**
 * @brief Remove head from list.
 * Remove head from list pointed by listPtr and set
 * listPtr to the next element in the list. If the head
 * was the last element of the list then the listPtr
 * is set to NULL
 * @param[in,out] listPtr    – pointer to list to be changed
 */
void removeHeadFromList(list_t *listPtr);

/**
 * @brief Remove last node from list.
 * Remove last node from the specified list. When the
 * last node is the first one sets the
 * @param[in,out] listPtr   – pointer to the list from which
 *                            last node is removed
 */
void removeLastFromList(list_t *listPtr);

/**
 * @brief Copy list data to empty list.
 * Copy all pointers from one list to another and creates
 * the same list in the same order. Data to which pointers
 * point to is not copied.
 * @param[in] from      – list (head pointer) to be copied
 * @param[in,out] toPtr – pointer tolist (head pointer)
 *                         to which data is copied
 * @return              – @p true when no memory errors occured,
 *                        otherwise return @p false and set
 *                        @p toPtr to @p NULL
 */
bool copyList(list_t from, list_t *toPtr);

/**
 * @brief Copy list data to empty list in reversed order.
 * Copy all pointers from one list to another and creates
 * list in reversed order. Data to which points the pointers
 * in list is not copied.
 * @param[in] from      – list (head pointer) to be copied
 * @param[in,out] toPtr – pointer to list (head pointer)
 *                         to which data is copied
 * @return              – @p true when no memory errors occured,
 *                        otherwise return @p false and set
 *                        @p toPtr to @p NULL
 */
bool copyListReversed(list_t from, list_t *toPtr);

/**
 * @brief Get last node of list.
 * Get the last node of the list by iterating over it.
 * Returns @p NULL when given list is empty.
 * @param[in] list      – list (head pointer) which end we
 *                        want to access
 * @return              – list (head pointer) of the last node
 *                        of the given list
 */
list_t getLastListNode(list_t list);

/**
 * @brief Get the one before last node in list.
 * Get the one before last node in list pointer
 * by iterating over the list and returning the
 * pointer to the specified node.
 * @param[in] list      – list (head pointer) which node we
 *                        want to access
 * @return              – list (head pointer) of the last node
 *                        of the given list
 */
list_t getOneBeforeLastListNode(list_t list);

/**
 * @brief Check if data exists in lists
 * Iterate over list and check if same pointer data in list is equal to the
 * pointer given to function to be search in list
 * @param[in] list      – list to search for pointer
 * @param[in] data1     – first pointer to search for
 * @param[in] data2     – second pointer to search for
 * @return              – true when found otherwise false
 */
bool existsInList(list_t list, void *data1, void *data2);

/**
 * @brief Free list and memory to which points list nodes.
 * Free the list nodes and the structures to which
 * the pointers included in list points to.
 * Uses the user defined/chose function to free the
 * structure to which the pointers in list point to
 * @param[in] list        – list (head pointer) to be freed
 * @param[in] freeNodeFun – pointer to function
 *                          which can free pointed structures
 */
void freeListWithContent(list_t list, void (*freeNodeFun)(void *));

/**
 * @brief Free list nodes only.
 * Free the list nodes only leaving the structures
 * to which the pointers in list points to untouched
 * @param[in] list        – list (head pointer) to be freed
 */
void freeList(list_t list);

#endif //IPP_ROADS_LIST_H
