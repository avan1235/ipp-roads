/** @file
 * Implementation of the class keeping the graph data structure
 * which is connected with some city tree.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "graph.h"

/**
 * @brief DFS mark vertices in graph.
 * Mark in the given array the vertices which can be achieved  from the
 * current vertex. Uses only vertices specified in given array or
 * all vertices when specified  in function argument
 * @param[in] graph                 – graph to search for vertices
 * @param[in] currentVertexNumber   – current marking vertex number
 * @param[in,out] visited           – array of visited (marked true) nodes
 * @param[in] canUseAllVertices     – if true all vertices can be used and
 *                                    canBeUsed may be set to NULL
 * @param[in] canBeUsed             – array of vertices that can be used in
 *                                    marking process
 */
static void DFSMark(graph_t *graph, unsigned currentVertexNumber, bool *visited,
                    bool canUseAllVertices, const bool *canBeUsed)
{
    graph_vertex_t *currentVertex = getGraphVertexPtr(graph, currentVertexNumber);
    visited[currentVertexNumber] = true;
    list_t edges = currentVertex->edges;

    while (edges != NULL) {
        unsigned nextNodeNumber = ((graph_edge_t *) edges->data)->adjacentTo;
        edges = edges->next;

        if (!visited[nextNodeNumber] && (canUseAllVertices || canBeUsed[nextNodeNumber]))
            DFSMark(graph, nextNodeNumber, visited, canUseAllVertices, canBeUsed);
    }
}

/**
 * @brief Check for path existence in graph.
 * Check if there exists in graph nay path from one vertex to another by using
 * specified vertices or all of them. Returns false when error in memory allocation.
 * @param[in] graph                 – graph to mark the vertices
 * @param[in] from                  – begin of path in graph
 * @param[in] to                    – end of path in graph
 * @param[in] canUseAllVertices     – true when can traverse using all vertices
 * @param[in] canBeUsed             – when canUseAllVertices is false this array
 *                                    is check if graph vertex can be visited
 * @return                          – true when path exists otherwise false
 */
static bool existsPathUsingVerticesHelp(graph_t *graph, unsigned from, unsigned to,
                                        bool canUseAllVertices, const bool *canBeUsed)
{
    if (graph == NULL)
        return false;

    bool *visitedNodes = (bool *) mem_alloc(
            graph->numberOfVertices * sizeof(bool));

    if (visitedNodes == NULL)
        return false;

    for (unsigned i = 0; i < graph->numberOfVertices; ++i)
        visitedNodes[i] = false;

    DFSMark(graph, from, visitedNodes, canUseAllVertices, canBeUsed);
    bool areConnected = visitedNodes[from] && visitedNodes[to];
    mem_free(visitedNodes);

    return areConnected;
}

/**
 * @brief Check for path existence in graph.
 * Check if there exists in graph any path from one vertex to another by using
 * specified vertices. Returns false when error in memory allocation.
 * @param[in] graph                 – graph to mark the vertices
 * @param[in] from                  – begin of path in graph
 * @param[in] to                    – end of path in graph
 * @param[in] canBeUsed             – when canUseAllVertices is false this array
 *                                    is check if graph vertex can be visited
 * @return                          – true when path exists otherwise false
 */
static bool existsPathUsingVertices(graph_t *graph, unsigned from, unsigned to, const bool *canBeUsed)
{
    return existsPathUsingVerticesHelp(graph, from, to, false, canBeUsed);
}

/**
 * @brief Check for path existence in graph.
 * Check if there exists in graph nay path from one vertex to another by using
 * all vertices. Returns false when error in memory allocation.
 * @param[in] graph                 – graph to mark the vertices
 * @param[in] from                  – begin of path in graph
 * @param[in] to                    – end of path in graph
 *                                    is check if graph vertex can be visited
 * @return                          – true when path exists otherwise false
 */
static bool existsPath(graph_t *graph, unsigned from, unsigned to)
{
    return existsPathUsingVerticesHelp(graph, from, to, true, NULL);
}

/**
 * @brief Compare two pairs of numbers.
 * Check if two pairs of numbers (p1, p2) and (v1, v2) are different when
 * they are unordered pairs.
 * @param[in] p1                    – first element of first pair
 * @param[in] p2                    – second element of first pair
 * @param[in] v1                    – first element of second pair
 * @param[in] v2                    – second element of second pair
 * @return                          – true when pairs are the same otherwise false
 */
static bool areDifferentPairsOfNumbers(unsigned p1, unsigned p2, unsigned v1, unsigned v2)
{
    return (!(p1 == v1 && p2 == v2)) && (!(p2 == v1 && p1 == v2));
}

graph_vertex_t* getGraphVertexPtr(graph_t *graph, unsigned index)
{
    if (graph != NULL && index < graph->numberOfVertices) {
        return  (graph_vertex_t *) getFromVector(graph->vertices, index);
    }
    return NULL;
}

graph_t *initGraph(void)
{
    graph_t *graph = (graph_t *) mem_alloc(sizeof(graph_t));

    if (graph != NULL) {
        graph->vertices = initVector();

        if (graph->vertices != NULL) {
            graph->numberOfVertices = 0;
            return graph;
        }
        else {
            mem_free(graph);
            return NULL;
        }
    }
    return NULL;
}

bool insertNewVertex(graph_t *graph, unsigned cityNameLength, tree_t endOfName)
{
    if (graph != NULL) {
        graph_vertex_t *newVertexPtr = (graph_vertex_t *) mem_alloc(
                sizeof(graph_vertex_t));

        if (newVertexPtr == NULL)
            return false;

        newVertexPtr->cityNameLength = cityNameLength;
        newVertexPtr->endOfName = endOfName;
        newVertexPtr->edges = NULL;

        if (appendToVector(graph->vertices, newVertexPtr)) {
            graph->numberOfVertices += 1;
            return true;
        }
        else {
            mem_free(newVertexPtr);
            return false;
        }
    }
    return false;
}

bool insertNewEdge(graph_t *graph, unsigned a, unsigned b, unsigned length, int buildYear)
{
    if (graph != NULL) {
        if (getGraphVertexPtr(graph, a) == NULL || getGraphVertexPtr(graph, b) == NULL)
            return false;

        graph_edge_t *newEdgeABPtr = (graph_edge_t *) mem_alloc(
                sizeof(graph_edge_t));
        graph_edge_t *newEdgeBAPtr = (graph_edge_t *) mem_alloc(
                sizeof(graph_edge_t));

        if (newEdgeABPtr == NULL || newEdgeBAPtr == NULL)
            goto HANDLE_ERROR;

        newEdgeABPtr->adjacentTo = b;
        newEdgeBAPtr->adjacentTo = a;

        newEdgeABPtr->buildYear = newEdgeBAPtr->buildYear = buildYear;
        newEdgeABPtr->length = newEdgeBAPtr->length = length;

        if (!insertToList(&((getGraphVertexPtr(graph, a))->edges), newEdgeABPtr))
            goto HANDLE_ERROR;

        if (!insertToList(&(getGraphVertexPtr(graph, b)->edges), newEdgeBAPtr)) {
            removeFromList(&(getGraphVertexPtr(graph, a)->edges), newEdgeABPtr);
            goto HANDLE_ERROR;
        }
        return true;

        HANDLE_ERROR:
        {
            mem_free(newEdgeABPtr);
            mem_free(newEdgeBAPtr);
            return false;
        }
    }
    return false;
}

unsigned getGraphEdgePtrLength(graph_edge_t *edge)
{
    return edge != NULL ? edge->length : (unsigned) -1;
}

int getGraphEdgePtrYear(graph_edge_t *edge)
{
    return edge != NULL ? edge->buildYear : 0;
}

unsigned cityNameLengthOfVertexPtr(graph_vertex_t *vertex)
{
    return vertex != NULL ? vertex->cityNameLength : 0;
}

unsigned numberOfVertices(graph_t *graph)
{
    return graph != NULL ? graph->numberOfVertices : 0;
}

bool isAdjacentTo(graph_t *graph, unsigned from, unsigned to, graph_edge_t **foundEdgePtr)
{
    if (graph != NULL) {
        graph_vertex_t *vertexA = getGraphVertexPtr(graph, from);
        if (vertexA == NULL)
            return false;
        list_t adjA = vertexA->edges;

        while (adjA != NULL) {
            if (((graph_edge_t *) (adjA->data))->adjacentTo == to) {
                *foundEdgePtr = (graph_edge_t *) (adjA->data);
                return true;
            }
            adjA = adjA->next;
        }
    }
    *foundEdgePtr = NULL;
    return false;
}

bool isAdjacentToCheck(graph_t *graph, unsigned from, unsigned to)
{
    graph_edge_t *tempEdgePtr = NULL;
    return isAdjacentTo(graph, from, to, &tempEdgePtr);
}

graph_edge_t *getGraphEdgePtr(graph_t *graph, unsigned from, unsigned to)
{
    graph_edge_t *tempEdgePtr = NULL;
    return isAdjacentTo(graph, from, to, &tempEdgePtr) ? tempEdgePtr : NULL;
}

int getGraphEdgeYear(graph_t *graph, unsigned from, unsigned to)
{
    graph_edge_t *tempEdgePtr = NULL;
    if (isAdjacentTo(graph, from, to, &tempEdgePtr))
        return tempEdgePtr->buildYear;
    else
        return 0;
}

vector_t *findShortestPathUsingNodesOmitEdge
        (graph_t *graph, unsigned from, unsigned to,
         bool canUseAllVertices, const bool *canVertexBeUsed,
         bool canUseAllEdges, unsigned edgeB, unsigned edgeE)
{
    if ((canUseAllVertices && !existsPath(graph, from, to))
        || (!canUseAllVertices && !existsPathUsingVertices(graph, from, to, canVertexBeUsed)))
        return NULL;

    struct {
        bool verticesHeapMemory;
        unsigned numberOfVectorsAllocated;
    } memoryAllocated = {0, 0};

    unsigned verticesNumber = graph->numberOfVertices;
    uint64_t distancesFrom[verticesNumber];
    min_heap_t *verticesHeap = initMinHeap(verticesNumber);
    vector_t *shortestPathsTo[verticesNumber];

    if (verticesHeap == NULL) goto HANDLE_ERROR;
    else memoryAllocated.verticesHeapMemory = true;

    for (unsigned i = 0; i < verticesNumber; ++i) {
        distancesFrom[i] = (i == from) ? 0 : (unsigned) -1;
        verticesHeap->nodes_array[i] = NULL;
        verticesHeap->positions[i] = i;
        shortestPathsTo[i] = NULL;
    }

    for (unsigned i = 0; i < verticesNumber; ++i) {
        verticesHeap->nodes_array[i] = initHeapNode(i, distancesFrom[i]);
        if (verticesHeap->nodes_array[i] == NULL) goto HANDLE_ERROR;
        else verticesHeap->size += 1;

        shortestPathsTo[i] = initVector();
        if (shortestPathsTo[i] == NULL) goto HANDLE_ERROR;
        else memoryAllocated.numberOfVectorsAllocated += 1;
    }
    decreaseKey(verticesHeap, from, distancesFrom[from]);
    list_t startList = NULL;
    if (!insertToList(&startList, getGraphVertexPtr(graph, from))) goto HANDLE_ERROR;
    if (!appendToVector(shortestPathsTo[from], startList)) {
        freeList(startList);
        goto HANDLE_ERROR;
    }

    while (!isMinHeapEmpty(verticesHeap)) {
        min_heap_node_t *minNode = extractMinNodeFromHeap(verticesHeap);
        unsigned minNodeNum = minNode->number;
        vector_t *pathsToMin = shortestPathsTo[minNodeNum];
        graph_vertex_t *minVertex = getGraphVertexPtr(graph, minNodeNum);
        list_t edges = minVertex->edges;

        while (edges != NULL) {
            graph_edge_t *currentEdge = (graph_edge_t *) edges->data;
            unsigned toNodeNum = currentEdge->adjacentTo;

            if ((canUseAllVertices || canVertexBeUsed[toNodeNum]) && isInMinHeap(verticesHeap, toNodeNum)
                && (canUseAllEdges || areDifferentPairsOfNumbers(minNodeNum, toNodeNum, edgeB, edgeE))
                && distancesFrom[minNodeNum] != (unsigned long) -1) {

                unsigned long newDistance = currentEdge->length + distancesFrom[minNodeNum];
                if (newDistance <= distancesFrom[toNodeNum]) {
                    if (newDistance < distancesFrom[toNodeNum]) {
                        freeVectorContent(shortestPathsTo[toNodeNum], (void (*)(void*)) &freeList);
                    }
                    for (unsigned i = 0; i < sizeOfVector(pathsToMin); ++i) {
                        list_t newPath = NULL;

                        if (!copyList(getFromVector(pathsToMin, i), &newPath)) goto HANDLE_ERROR;
                        if (!insertToList(&newPath, getGraphVertexPtr(graph, toNodeNum))) {
                            freeList(newPath);
                            goto HANDLE_ERROR;
                        }
                        if (!appendToVector(shortestPathsTo[toNodeNum], newPath)) {
                            freeList(newPath);
                            goto HANDLE_ERROR;
                        }
                    }
                    distancesFrom[toNodeNum] = newDistance;
                    decreaseKey(verticesHeap, toNodeNum, distancesFrom[toNodeNum]);
                }
            }
            edges = edges->next;
        }
        mem_free(minNode);
    }

    if (distancesFrom[to] == (unsigned long) -1)
        goto HANDLE_ERROR;

    vector_t *returnVector = shortestPathsTo[to];
    freeMinHeap(verticesHeap);
    for (unsigned k = 0; k < verticesNumber; ++k) {
        if (k != to) {
            freeVectorContent(shortestPathsTo[k], (void (*)(void*)) &freeList);
            freeVector(shortestPathsTo[k]);
        }
    }
    return returnVector;

    HANDLE_ERROR:
    {
        if (memoryAllocated.verticesHeapMemory) freeMinHeap(verticesHeap);
        for (unsigned i = 0; i < memoryAllocated.numberOfVectorsAllocated; ++i) {
            freeVectorContent(shortestPathsTo[i], (void (*)(void*)) &freeList);
            freeVector(shortestPathsTo[i]);
        }
        return NULL;
    }
}

unsigned getGraphVertexNumber(graph_vertex_t *vertex)
{
    return vertex->endOfName->splitChar == END_OF_STRING ?
           vertex->endOfName->next.graph_node_number :
           (unsigned) -1;
}

graph_vertex_t *getExistingGraphVertexPtr(graph_t *graph, const char *city, tree_t tree)
{
    unsigned nodeNum;
    if (!searchInTree(tree, city, &nodeNum))
        return NULL;

    return getGraphVertexPtr(graph, nodeNum);
}

unsigned getExistingGraphVertexNumber(graph_t *graph, const char *city, tree_t tree)
{
    if (graph == NULL || city == NULL)
        return (unsigned) -1;

    graph_vertex_t *v = getExistingGraphVertexPtr(graph, city, tree);

    if (v == NULL) return (unsigned) -1;
    else return v->endOfName->next.graph_node_number;
}

graph_edge_t *getExistingGraphEdgePtr(graph_t *graph, graph_vertex_t *from, graph_vertex_t *to)
{
    list_t adj = from->edges;
    while (adj != NULL) {
        if (getGraphVertexPtr(graph, ((graph_edge_t *) adj->data)->adjacentTo) == to)
            return adj->data;
        adj = adj->next;
    }
    return NULL;
}

void freeAllGraphData(graph_t *graph)
{
    if (graph != NULL) {
        for (unsigned i = 0; i < graph->numberOfVertices; ++i) {
            graph_vertex_t *v = getGraphVertexPtr(graph, i);
            list_t adjV = v->edges;
            freeListWithContent(adjV, &mem_free);
            mem_free(v);
        }
        freeVector(graph->vertices);
        mem_free(graph);
    }
}