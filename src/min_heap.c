/** @file
 * Implementation of the class min heap data structure
 * to be used as the priority queue.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "min_heap.h"

/**
 * @brief Swap two nodes of min heap in array.
 * @param[in] a     – first of values to be swapped
 * @param[in] b     – second of values to be swapped
 */
static void swapMinHeapNodes(min_heap_node_t **a, min_heap_node_t **b)
{
    min_heap_node_t *temp = *a;
    *a = *b;
    *b = temp;
}

/**
 * @brief Keep the min heap property of heap.
 * Restore the min heap property of special min heap nodes
 * order to keep the nodes of the smallest priority at begin of heap.
 * @param[in] minHeap   – min heap to be changed
 * @param[in] num       – index in heap from process starts
 */
static void heapify(min_heap_t *minHeap, unsigned num)
{
    unsigned smallest, left, right;
    smallest = num;
    left = 2 * num + 1;
    right = 2 * num + 2;

    if (left < minHeap->size
        && minHeap->nodes_array[left]->priority
                < minHeap->nodes_array[smallest]->priority)
        smallest = left;

    if (right < minHeap->size
        && minHeap->nodes_array[right]->priority
                < minHeap->nodes_array[smallest]->priority)
        smallest = right;

    if (smallest != num) {
        min_heap_node_t *smallestNode = minHeap->nodes_array[smallest];
        min_heap_node_t *idxNode = minHeap->nodes_array[num];
        minHeap->positions[smallestNode->number] = num;
        minHeap->positions[idxNode->number] = smallest;
        swapMinHeapNodes(&minHeap->nodes_array[smallest], &minHeap->nodes_array[num]);
        heapify(minHeap, smallest);
    }
}

min_heap_node_t* initHeapNode(unsigned nodeNumber, priority_t priority)
{
    min_heap_node_t *newNodePtr = (min_heap_node_t *) mem_alloc(
            sizeof(min_heap_node_t));

    if (newNodePtr == NULL)
        return NULL;

    newNodePtr->number = nodeNumber;
    newNodePtr->priority = priority;
    return newNodePtr;
}

min_heap_t *initMinHeap(unsigned capacity)
{
    min_heap_t *newHeapPtr = (min_heap_t *) mem_alloc(sizeof(min_heap_t));

    if (newHeapPtr == NULL)
        return NULL;

    newHeapPtr->size = 0;
    newHeapPtr->positions = (unsigned *) mem_alloc(sizeof(unsigned) * capacity);

    if (newHeapPtr->positions == NULL) {
        mem_free(newHeapPtr);
        return NULL;
    }
    newHeapPtr->nodes_array = (min_heap_node_t **) mem_alloc(
            sizeof(min_heap_node_t *) * capacity);

    if (newHeapPtr->nodes_array == NULL) {
        mem_free(newHeapPtr->positions);
        mem_free(newHeapPtr);
        return NULL;
    }
    return newHeapPtr;
}

bool isMinHeapEmpty(min_heap_t *minHeap)
{
    return minHeap->size == 0;
}

min_heap_node_t *extractMinNodeFromHeap(min_heap_t *minHeap)
{
    if (isMinHeapEmpty(minHeap))
        return NULL;

    min_heap_node_t *root = minHeap->nodes_array[0];
    min_heap_node_t *lastNode = minHeap->nodes_array[minHeap->size - 1];
    minHeap->nodes_array[0] = lastNode;
    minHeap->positions[root->number] = minHeap->size-1;
    minHeap->positions[lastNode->number] = 0;
    minHeap->size -= 1;
    heapify(minHeap, 0);

    return root;
}

void decreaseKey(min_heap_t *minHeap, unsigned number, priority_t priority)
{
    if (minHeap == NULL)
        return;

    unsigned i = minHeap->positions[number];
    minHeap->nodes_array[i]->priority = priority;

    while (i && minHeap->nodes_array[i]->priority < minHeap->nodes_array[(i - 1) / 2]->priority) {
        minHeap->positions[minHeap->nodes_array[i]->number] = (i - 1) / 2;
        minHeap->positions[minHeap->nodes_array[(i - 1) / 2]->number] = i;
        swapMinHeapNodes(&minHeap->nodes_array[i],  &minHeap->nodes_array[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

bool isInMinHeap(min_heap_t *minHeap, unsigned number)
{
    if (minHeap == NULL || minHeap->positions[number] >= minHeap->size)
        return false;

    return true;
}

void freeMinHeap(min_heap_t *minHeap)
{
    if (minHeap != NULL) {
        while (!isMinHeapEmpty(minHeap)) {
            mem_free(extractMinNodeFromHeap(minHeap));
        }
        mem_free(minHeap->positions);
        mem_free(minHeap->nodes_array);
        mem_free(minHeap);
    }
}
