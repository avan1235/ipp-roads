/** @file
 * Implementation of the class of country routes.
 *
 * @author Lukasz Kaminski <kamis@mimuw.edu.pl>, Marcin Peczarski <marpe@mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 20.03.2019
 */

#include "map.h"

/**
 * @brief Validate city name.
 * Check if given string is a valid city name so has length not
 * equal to zero and does not include disallowed characters
 * @param[in] city   – city name to be checked
 * @return           – true when valid otherwise false
 */
static bool isValidNameCity(const char *city)
{
    if (strlen(city) < 1)
        return false;

    for (unsigned i = 0; i < strlen(city); ++i) {
        if ((*(city+i) <= MAX_CHAR_OUT_VALUE && *(city+i) >= MIN_CHAR_OUT_VALUE)
            || *(city+i) == SEPARATE_SYMBOL)
            return false;
    }
    return true;
}

/**
 * @brief Validate year value.
 * Check if year value is different than 0
 * @param[in] builtYear – year value to validate
 * @return              – true when valid otherwise false
 */
static bool isValidYear(int builtYear)
{
    return builtYear != 0;
}

/**
 * @brief Validate id of route.
 * Check if given id is from range [1, ..., 999]
 * @param[in] id    – number to check
 * @return          – true when valid otherwise false
 */
static bool isValidRouteId(unsigned id)
{
    return id >= MIN_ROUTE_ID && id <= MAX_ROUTE_ID;
}

/**
 * @brief Compare two cities names.
 * Check if two cities names are different cities names
 * by comparing them as the strings.
 * @param[in] city1 – first of cities to compare
 * @param[in] city2 – second of cities to compare
 * @return          – true when different names otherwise false
 */
static bool areDifferentCitiesNames(const char *city1, const char *city2)
{
    return strcmp(city1, city2) != 0;
}

/**
 * @brief Free the new inserted nodes to cities tree.
 * @param[in] newNodes          – array of num inserted nodes to tree
 * @param[in] insertedNumber    – number of new inserted nodes
 */
static void freeLastTreeInsert(tree_t *newNodes, unsigned insertedNumber)
{
    if (newNodes != NULL) {
        for (unsigned i = 0; i < insertedNumber; ++i) {
            mem_free(*(newNodes + i));
        }
    }
}

/**
 * @brief Free backup arrays of tree nodes
 * @param[in] newNodes1         – first array to be freed
 * @param[in] newNodes2         – second array to be freed
 */
static void freeBackupTreeNodes(tree_t *newNodes1, tree_t *newNodes2)
{
    mem_free(newNodes1);
    mem_free(newNodes2);
}

/**
 * @brief Get min of two values
 * @param[in] a     – first value
 * @param[in] b     – second value
 * @return          – min(a, b)
 */
static int minOfInts(int a, int b)
{
    return (a <= b) ? a : b;
}

/**
 * @brief Get max of two values
 * @param[in] a     – first value
 * @param[in] b     – second value
 * @return          – max(a, b)
 */
static int maxOfInts(int a, int b)
{
    return (a >= b) ? a : b;
}

/**
 * @brief Get the year of the oldest road in route
 * Search in all edges between given vertices for the oldest road
 * @param[in] graph         – graph to search for vertices
 * @param[in] citiesOnRoad  – list of pointers to vertices
 * @return                  – build year of oldest road
 */
static int oldestRoad(graph_t *graph, list_t citiesOnRoad)
{
    list_t last = citiesOnRoad;
    list_t next = citiesOnRoad->next;

    int oldestRoad = INT_MAX;

    while (next != NULL) {
        int newYear = getGraphEdgeYear(graph,
                                       getGraphVertexNumber(last->data),
                                       getGraphVertexNumber(next->data));
        oldestRoad = minOfInts(oldestRoad, newYear != 0 ? newYear : INT_MAX);
        last = next;
        next = next->next;
    }
    return oldestRoad;
}

/**
 * @brief Get the number of digits of signed number.
 * For negative values counts '-' as separate digit
 * @param[in] x     – number to determine number of digits
 * @return          – number of digits of specified number
 */
static unsigned digits(long long x)
{
    unsigned digits = 0;
    if (x < 0) digits += 1;
    while (x != 0) {
        x /= 10;
        digits++;
    }
    return digits;
}

/**
 * @brief Get summary length of roads in list of edges.
 * @param[in] edges     – graph edges list to sum their length
 * @return              – summary length of edges
 */
static unsigned summaryLength(list_t edges)
{
    unsigned result = 0;
    while (edges != NULL) {
        result += getGraphEdgePtrLength(edges->data);
        edges = edges->next;
    }
    return result;
}

/**
 * @brief Get summary length of roads in list of cities.
 * Vertices and edges in graph have to exist when use this function
 * @param[in] cities    – graph vertices list to sum length between them
 * @param[in] graph     – graph to search for vertices
 * @return              – summary length of edges
 */
static unsigned summaryLengthForCities(list_t cities, graph_t *graph)
{
    unsigned result = 0;
    if (cities != NULL) {
        while (cities->next != NULL) {
            result += getExistingGraphEdgePtr(graph, cities->data, cities->next->data)->length;
            cities = cities->next;
        }

    }
    return result;
}

/**
 * @brief Get length of the string of route description
 * Browse all cities and roads in route and determine the length
 * of the string that describes given route.
 * @param[in] map       – map in which is route
 * @param[in] routeId   – number of route to check
 * @return              – size of route description string
 */
static size_t sizeOfRouteDescription(Map *map, unsigned routeId)
{
    size_t size = 0;
    size += digits(routeId);
    size += SEPARATE_SIZE;

    list_t roads = map->routes[routeId - 1]->roads;
    list_t cities = map->routes[routeId - 1]->cities;

    while (roads != NULL) {
        size += cityNameLengthOfVertexPtr(cities->data);
        size += SEPARATE_SIZE;
        size += digits(getGraphEdgePtrLength(roads->data));
        size += SEPARATE_SIZE;
        size += digits(getGraphEdgePtrYear(roads->data));
        size += SEPARATE_SIZE;

        roads = roads->next;
        cities = cities->next;
    }
    size += cityNameLengthOfVertexPtr(cities->data);
    return size;
}

/**
 * @brief Mark vertices not used by route
 * Check for route cities numbers and check used and not used ones
 * in an array of bools.
 * @param[in] map               – map to check for routes
 * @param[in] routeId           – id of route to mark
 * @param[in,out] freeVertices  – array to be set by mark function
 */
static void markNotUsedVerticesByRoute(Map *map, unsigned routeId, bool *freeVertices)
{
    for (unsigned i = 0; i < numberOfVertices(map->graph); ++i)
        freeVertices[i] = true;

    list_t cities = map->routes[routeId - 1]->cities;
    while (cities != NULL) {
        freeVertices[getGraphVertexNumber(cities->data)] = false;
        cities = cities->next;
    }
}

/**
 * @brief Create new route using best path cities.
 * Build new route by traversing through all cities from shortest path
 * @param[in] map               – pointer to map structure
 * @param[in,out] newRoute      – pointer to route to be changed
 * @param[in] cities            – list of cities on best path
 * @return                      – true when successfully create new route
 *                                using all vertices from bestParents,
 *                                otherwise false
 */
static bool recoverBestPath(Map *map, route_t *newRoute, list_t cities) {
    while (cities->next != NULL) {
        graph_vertex_t *v1 = cities->data;
        graph_vertex_t *v2 = cities->next->data;
        graph_edge_t *e = getExistingGraphEdgePtr(map->graph, v2, v1);
        if (!insertToList(&(newRoute->cities), v1) || !insertToList(&(newRoute->roads), e))
            return false;
        cities = cities->next;
    }
    return insertToList(&(newRoute->cities), cities->data);
}

/**
 * @brief Validate params for new route via command
 * Validate params by checking if there are no cycles in given route,
 * all params are valid, every length and year of build is correct
 * or can be changed to new value.
 * @param[in,out] map        – pointer to map structure
 * @param[in] cities         – array of cities names to create reoute via
 * @param[in] years          – array of build/repair years of roads
 * @param[in] lengths        – array of lengths of roads
 * @param[in] howManyCities  – number of cities in new route
 * @return                   – true when route can be created otherwise false
 */
static bool validRouteParams(Map *map, const char **cities, int *years, unsigned *lengths, unsigned howManyCities)
{
    tree_t  checkTree = NULL;
    unsigned nodeNum;

    for (unsigned i = 0; i < howManyCities - 1; ++i) {
        if (!isValidNameCity(cities[i]) || !isValidYear(years[i]) || lengths[i] == 0)
            goto ERROR;

        if (!searchInTree(checkTree, cities[i], &nodeNum)) {
            tree_t leaf = NULL;
            unsigned insertedNodesToTree = 0;
            tree_t  *newNodes = (tree_t *) mem_alloc(
                    sizeof(tree_t) * (strlen(cities[i]) + 1));
            if (newNodes == NULL)
                goto ERROR;

            if (!insertToTree(&checkTree, cities[i], i, &leaf, newNodes, &insertedNodesToTree)) {
                freeLastTreeInsert(newNodes, insertedNodesToTree);
                mem_free(newNodes);
                goto ERROR;
            }
            mem_free(newNodes);

            graph_vertex_t *city1 = getExistingGraphVertexPtr(map->graph, cities[i], map->cities);
            graph_vertex_t *city2 = getExistingGraphVertexPtr(map->graph, cities[i+1], map->cities);
            if (city1 == NULL || city2 == NULL)
                continue;

            graph_edge_t *road = getExistingGraphEdgePtr(map->graph, city1, city2);
            if (road == NULL)
                continue;

            if (road->length != lengths[i] || road->buildYear > years[i])
                goto ERROR;
        }
        else goto ERROR;
    }

    if (!isValidNameCity(cities[howManyCities - 1])) goto ERROR;
    if (searchInTree(checkTree, cities[howManyCities - 1], &nodeNum)) goto ERROR;

    freeTree(checkTree);
    return true;

    ERROR:
    {
        freeTree(checkTree);
        return false;
    }
}

/**
 * @brief Find new route between two cities
 * In route can be used specified vertices and has to be omitted
 * the specified graph edge when search for best path.
 * @param[in] map                   – map to search for new route
 * @param[in] nodeNum1              – start route node number
 * @param[in] nodeNum2              – end route node number
 * @param[in] canUseAllVertices     – when can use all vertices set to true
 *                                    otherwise false
 * @param[in] canVertexBeUsed       – array of parametrized to use vertices
 * @param[in] canUseAllEdges        – when can use all edges set to true
 *                                    otherwise false
 * @param[in] edgeBegin             – begin of edge that cannot be used
 * @param[in] edgeEnd               – end of edge that cannot be used
 * @param[in,out] numberOfRoutes    – pointer to be set the number of routes
 *                                    found on shortest path
 * @param[in,out] length            – pointer to be set the shortest path length
 *                                    when numberOfRoutes is bigger than 0
 * @param[in,out] year              – pointer to be set the shortest path year
 *                                    when numberOfRoutes is bigger than 0
 * @return                          – new route pointer. When error in allocation
 *                                    memory or cannot find new route returns
 *                                    NULL pointer
 */
static route_t *newRouteUsingVerticesOmitEdgeSetNumberOfRoutesAndLengthHelp
        (Map *map, unsigned nodeNum1, unsigned nodeNum2, bool canUseAllVertices,
         const bool *canVertexBeUsed, bool canUseAllEdges, unsigned edgeBegin,
         unsigned edgeEnd, unsigned *numberOfRoutes, uint64_t *length, int *year)
{
    if (numberOfRoutes != NULL)
        *numberOfRoutes = 0;

    if (year != NULL)
        *year = 0;

    if (length != NULL)
        *length = (unsigned) -1;

    list_t bestCitiesRoute = NULL;

    vector_t *shortestPaths =
            findShortestPathUsingNodesOmitEdge(map->graph, nodeNum1, nodeNum2,
                                               canUseAllVertices, canVertexBeUsed,
                                               canUseAllEdges, edgeBegin, edgeEnd);
    if (numberOfRoutes != NULL)
        *numberOfRoutes = sizeOfVector(shortestPaths);

    if (shortestPaths == NULL || sizeOfVector(shortestPaths) == 0) goto HANDLE_ERROR;

    if (sizeOfVector(shortestPaths) > 1) {
        if (length != NULL)
            *length = summaryLengthForCities(getFromVector(shortestPaths, 0), map->graph);

        unsigned numberOfPossiblePaths = sizeOfVector(shortestPaths);
        int oldestRoads[numberOfPossiblePaths];
        for (unsigned i = 0; i < numberOfPossiblePaths; ++i)
            oldestRoads[i] = oldestRoad(map->graph, getFromVector(shortestPaths, i));

        int youngest = INT_MIN;
        for (unsigned i = 0; i < numberOfPossiblePaths; ++i)
            youngest = maxOfInts(youngest, oldestRoads[i]);

        unsigned howManyYoungest = 0;
        unsigned lastYoungestIndex = 0;
        for (unsigned i = 0; i < numberOfPossiblePaths; ++i)
            if (youngest == oldestRoads[i]) {
                howManyYoungest += 1;
                lastYoungestIndex = i;
            }

        if (year != NULL)
            *year = oldestRoad(map->graph, getFromVector(shortestPaths, lastYoungestIndex));

        if (howManyYoungest > 1) {
            if (numberOfRoutes != NULL)
                *numberOfRoutes = howManyYoungest;
            goto HANDLE_ERROR;
        }
        else {
            bestCitiesRoute = getFromVector(shortestPaths, lastYoungestIndex);
            if (numberOfRoutes != NULL)
                *numberOfRoutes = 1;
        }
    }
    else {
        bestCitiesRoute = getFromVector(shortestPaths, 0);
        if (length != NULL)
            *length = summaryLengthForCities(bestCitiesRoute, map->graph);
        if (year != NULL)
            *year = oldestRoad(map->graph, bestCitiesRoute);
    }

    route_t *newRoute = initRoute();
    if (newRoute == NULL)
        goto HANDLE_ERROR;

    if (!recoverBestPath(map, newRoute, bestCitiesRoute)) {
        freeRoute(newRoute);
        goto HANDLE_ERROR;
    }
    else {
        freeVectorContent(shortestPaths, (void (*)(void*)) &freeList);
        freeVector(shortestPaths);
    }
    return newRoute;

    HANDLE_ERROR:
    {
        freeVectorContent(shortestPaths, (void (*)(void*)) &freeList);
        freeVector(shortestPaths);
        return NULL;
    }
}

/**
 * @brief Convert unsigned number to string with no END_OF_STRING
 * @param[in,out] str   – pointer to string to be created
 * @param[in] len       – length of created number string
 * @param[in] val       – number to convert to string
 */
static void unsignedToString(char *str, unsigned len, unsigned val)
{
    while (len > 0) {
        *(str + len - 1) = (char) ('0' + (val % 10));
        len -= 1;
        val /= 10;
    }
}

/**
 * @brief Convert signed number to string with no END_OF_STRING
 * @param[in,out] str   – pointer to string to be created
 * @param[in] len       – length of created number string
 * @param[in] val       – number to convert to string
 */
static void signedToString(char *str, unsigned len, int val)
{
    if (val < 0) {
        *str = '-';
        unsignedToString(str + 1, len - 1, (unsigned) ((-1) * val));
    }
    else {
        unsignedToString(str, len, (unsigned) val);
    }
}

/**
 * @brief Find new route between two cities
 * In route can be used specified vertices and has to be omitted
 * the specified graph edge when search for best path.
 * @param[in] map                   – map to search for new route
 * @param[in] nodeNum1              – start route node number
 * @param[in] nodeNum2              – end route node number
 * @param[in] canUseAllVertices     – when can use all vertices set to true
 *                                    otherwise false
 * @param[in] canVertexBeUsed       – array of parametrized to use vertices
 * @param[in] canUseAllEdges        – when can use all edges set to true
 *                                    otherwise false
 * @param[in] edgeBegin             – begin of edge that cannot be used
 * @param[in] edgeEnd               – end of edge that cannot be used
 * @return                          – new route pointer. When error in allocation
 *                                    memory or cannot find new route returns
 *                                    NULL pointer
 */
static route_t *newRouteUsingVerticesOmitEdgeHelp
        (Map *map, unsigned nodeNum1, unsigned nodeNum2, bool canUseAllVertices,
         const bool *canVertexBeUsed, bool canUseAllEdges, unsigned edgeBegin,
         unsigned edgeEnd)
{
    return newRouteUsingVerticesOmitEdgeSetNumberOfRoutesAndLengthHelp(map, nodeNum1, nodeNum2,
            canUseAllVertices, canVertexBeUsed, canUseAllEdges, edgeBegin, edgeEnd, NULL, NULL, NULL);
}

/**
 * @brief Find new route between two cities
 * In route can be used specified vertices and has to be omitted
 * the specified graph edge when search for best path.
 * @param[in] map                   – map to search for new route
 * @param[in] nodeNum1              – start route node number
 * @param[in] nodeNum2              – end route node number
 * @param[in] canBeUsed             – array of parametrized to use vertices
 * @param[in] edgeBegin             – begin of edge that cannot be used
 * @param[in] edgeEnd               – end of edge that cannot be used
 * @return                          – new route pointer. When error in allocation
 *                                    memory or cannot find new route returns
 *                                    NULL pointer
 */
static route_t *newRouteUsingVerticesOmitEdge
(Map *map, unsigned nodeNum1, unsigned nodeNum2, const bool *canBeUsed, unsigned edgeBegin, unsigned edgeEnd)
{
    return newRouteUsingVerticesOmitEdgeHelp(map, nodeNum1, nodeNum2, false, canBeUsed, false, edgeBegin, edgeEnd);
}

/**
 * @brief Find new route between two cities
 * In route can be used specified vertices.
 * @param[in] map                   – map to search for new route
 * @param[in] nodeNum1              – start route node number
 * @param[in] nodeNum2              – end route node number
 * @param[in] canBeUsed             – array of parametrized to use vertices
 * @param[in,out] numberOfRoutes    – pointer to be set the number of routes
 *                                    found on shortest path
 * @param[in,out] length            – pointer to be set the shortest path length
 *                                    when numberOfRoutes is bigger than 0
 * @param[in,out] year              – pointer to be set the shortest path year
 *                                    when numberOfRoutes is bigger than 0
 * @return                          – new route pointer. When error in allocation
 *                                    memory or cannot find new route returns
 *                                    NULL pointer
 */
static route_t *newRouteUsingVerticesSetNumberOfRoutesLengthAndYear
(Map *map, unsigned nodeNum1, unsigned nodeNum2, const bool *canBeUsed, unsigned *numberOfRoutes, uint64_t *length, int *year)
{
    return newRouteUsingVerticesOmitEdgeSetNumberOfRoutesAndLengthHelp(map, nodeNum1, nodeNum2, false, canBeUsed, true, 0, 0, numberOfRoutes, length, year);
}

/**
 * @brief Find new route between two cities
 * In route can be used all vertices and edges.
 * @param[in] map                   – map to search for new route
 * @param[in] nodeNum1              – start route node number
 * @param[in] nodeNum2              – end route node number
 * @return                          – new route pointer. When error in allocation
 *                                    memory or cannot find new route returns
 *                                    NULL pointer
 */
static route_t *newRouteAllVertices(Map *map, unsigned nodeNum1, unsigned nodeNum2)
{
    return newRouteUsingVerticesOmitEdgeHelp(map, nodeNum1, nodeNum2, true, NULL, true, 0, 0);
}

/**
 * @brief Get the number of routes in which exists road between two cities
 * Get the list of routes number to which belongs two cities given by graph
 * vertices pointers and which has any of the given of graph edges as the road
 * in route.
 * @param[in] edge12            – edge from first city to second
 * @param[in] edge21            – edge from second city to first
 * @param[in,out] howMany       – set the number of common routes of cities
 * @param[in] map               – map to search for routes
 * @return                      – list with common routes numbers
 */
static uint_list_t commonRoutesNeighboursNumbers
(graph_edge_t *edge12, graph_edge_t *edge21, unsigned *howMany, Map *map)
{
    uint_list_t result = NULL;
    *howMany = 0;
    for (unsigned i = 0; i < NUMBER_OF_ROUTES; ++i) {
        if ((map->routes)[i] != NULL) {
            route_t *commonRoute = (map->routes)[i];
            if (existsInList(commonRoute->roads, edge12, edge21)) {
                if (!insertToUIntList(&result, i+1)) {
                    *howMany = (unsigned) -1;
                    freeUIntList(result);
                    return NULL;
                }
                else *howMany += 1;
            }
        }
    }
    return result;
}

/**
 * @brief Find out which city is first in route
 * Search for any of given cities in route and when find and returns its number.
 * @param[in] route         – route to search for cities
 * @param[in] nodeNum1      – number of first city graph node
 * @param[in] nodeNum2      – number of second city graph node
 * @return                  – number of first of given cities in route. When
 *                            not found the any city returns UINT_MAX value.
 */
static unsigned firstCityInRouteNum(route_t *route, unsigned nodeNum1, unsigned nodeNum2)
{
    list_t vertex = route->cities;
    while (vertex != NULL) {
        if (getGraphVertexNumber(vertex->data) == nodeNum1
            || getGraphVertexNumber(vertex->data) == nodeNum2) {
            return getGraphVertexNumber(vertex->data);
        }
        vertex = vertex->next;
    }
    return (unsigned) -1;
}

/** @brief Change year of the last repair of road and can check length of road.
 * For the road between two cities change year of its last repair
 * or change this year if there was no repair.
 * @param[in,out] map    – pointer to map structure
 * @param[in] city1      – pointer to string of one of cities
 * @param[in] city2      – pointer to string of second of cities
 * @param[in] repairYear – new repair year of road
 * @param checkLength    – when length must be checked set to true
 * @param length         – length of road to be compared with already set one
 * @return               – true when successfully changed the value, false when
 *                         parameters has wrong values, some city doesn't exist,
 *                         there is no road between two cities, given year is
 *                         earlier the old one
 */
static bool repairRoadCanCheckLength(Map *map, const char *city1, const char *city2, int repairYear, bool checkLength, unsigned length)
{
    if (map == NULL || !isValidNameCity(city1) || !isValidNameCity(city2)
        || !isValidYear(repairYear) || !areDifferentCitiesNames(city1, city2)) {
        return false;
    }

    unsigned nodeNum1 = 0;
    unsigned nodeNum2 = 0;

    if (!searchInTree(map->cities, city1, &nodeNum1)
        || !searchInTree(map->cities, city2, &nodeNum2)) {
        return false;
    }

    graph_edge_t *edgeFrom1To2 = NULL;
    graph_edge_t *edgeFrom2To1 = NULL;

    if (!isAdjacentTo(map->graph, nodeNum1, nodeNum2, &edgeFrom1To2)
        || !isAdjacentTo(map->graph, nodeNum2, nodeNum1, &edgeFrom2To1)) {
        return false;
    }

    if (edgeFrom1To2->buildYear > repairYear || edgeFrom2To1->buildYear > repairYear
        || (checkLength && (length != edgeFrom1To2->length || length != edgeFrom2To1->length))) {
        return false;
    }

    edgeFrom1To2->buildYear = repairYear;
    edgeFrom2To1->buildYear = repairYear;

    return true;
}

Map* newMap(void)
{
    Map *newMap = (Map *) mem_alloc(sizeof(Map));

    if (newMap != NULL) {
        newMap->cities = NULL;
        newMap->graph = initGraph();
        newMap->routes = (route_t **) mem_alloc(
                NUMBER_OF_ROUTES * sizeof(route_t *));

        if (newMap->graph != NULL && newMap->routes != NULL) {
            for (unsigned i = 0; i < NUMBER_OF_ROUTES; ++i) {
                newMap->routes[i] = NULL;
            }
            return newMap;
        }
        else {
            freeAllGraphData(newMap->graph);
            mem_free(newMap->routes);
            mem_free(newMap);
        }
    }
    return NULL;
}

void deleteMap(Map *map)
{
    if (map != NULL) {
        freeAllGraphData(map->graph);
        freeTree(map->cities);

        for (unsigned i = 0; i < NUMBER_OF_ROUTES; ++i) {
            freeRoute(*(map->routes + i));
        }
        mem_free(map->routes);
        mem_free(map);
    }
}

bool addRoad(Map *map, const char *city1, const char *city2, unsigned length, int builtYear)
{
    if (map == NULL || !isValidNameCity(city1) || !isValidNameCity(city2) || length == 0
        || !isValidYear(builtYear) || !areDifferentCitiesNames(city1, city2))
        return false;

    bool success;

    unsigned nodeNum1 = 0;
    unsigned nodeNum2 = 0;

    unsigned currentNumberOfCities = numberOfVertices(map->graph);

    tree_t leaf1 = NULL;
    tree_t leaf2 = NULL;

    tree_t  *newNodes1 = (tree_t *) mem_alloc(
            sizeof(tree_t) * (strlen(city1) + 1));
    tree_t  *newNodes2 = (tree_t *) mem_alloc(
            sizeof(tree_t) * (strlen(city2) + 1));

    if (newNodes1 == NULL || newNodes2 == NULL)
        goto HANDLE_ERROR;

    unsigned insertedNodesToTree1 = 0;
    unsigned insertedNodesToTree2 = 0;

    bool nowInserted1 = false;
    bool nowInserted2 = false;

    if (!searchInTree(map->cities, city1, &nodeNum1)) {
        nowInserted1 = insertToTree(&(map->cities), city1, currentNumberOfCities, &leaf1, newNodes1, &insertedNodesToTree1);

        if (!nowInserted1) {
            freeLastTreeInsert(newNodes1, insertedNodesToTree1);
            goto HANDLE_ERROR;
        }
        else {
            nodeNum1 = currentNumberOfCities;
            currentNumberOfCities += 1;
        }
    }

    if (!searchInTree(map->cities, city2, &nodeNum2)) {
        nowInserted2 = insertToTree(&(map->cities), city2, currentNumberOfCities, &leaf2, newNodes2, &insertedNodesToTree2);

        if (!nowInserted2) {
            freeLastTreeInsert(newNodes2, insertedNodesToTree2);

            if (nowInserted1) {
                freeLastTreeInsert(newNodes1, insertedNodesToTree1);
            }
            goto HANDLE_ERROR;
        }
        else {
            nodeNum2 = currentNumberOfCities;
        }
    }

    if (nowInserted1) {
        success = insertNewVertex(map->graph, strlen(city1), leaf1);

        if (!success) {
            freeLastTreeInsert(newNodes1, insertedNodesToTree1);

            if (nowInserted2) {
                freeLastTreeInsert(newNodes2, insertedNodesToTree2);
            }
            goto HANDLE_ERROR;
        }
    }

    if (nowInserted2) {
        success = insertNewVertex(map->graph, strlen(city2), leaf2);

        if (!success) {
            freeLastTreeInsert(newNodes2, insertedNodesToTree2);

            if (nowInserted1) {
                freeLastTreeInsert(newNodes1, insertedNodesToTree1);
                mem_free((graph_vertex_t *) removeFromVector(
                        map->graph->vertices, nodeNum1));
            }
            goto HANDLE_ERROR;
        }
    }

    /**
     * When both cities were already in map check if there
     * is not already a road between them
     */
    if (!nowInserted1 && !nowInserted2
        && isAdjacentToCheck(map->graph, nodeNum1, nodeNum2)
            && isAdjacentToCheck(map->graph, nodeNum2, nodeNum1)) {
        goto HANDLE_ERROR;
    }

    /**
     * Vertices has already been inserted successfully
     * or were already in map and there is no road between
     *  them so we can work with them
     */
    if (!insertNewEdge(map->graph, nodeNum1, nodeNum2, length, builtYear)) {

        if (nowInserted1) {
            freeLastTreeInsert(newNodes1, insertedNodesToTree1);
            mem_free((graph_vertex_t *) removeFromVector(map->graph->vertices,
                                                         nodeNum1));
        }
        if (nowInserted2) {
            freeLastTreeInsert(newNodes2, insertedNodesToTree2);
            mem_free((graph_vertex_t *) removeFromVector(map->graph->vertices,
                                                         nodeNum2));
        }
        goto HANDLE_ERROR;
    }

    freeBackupTreeNodes(newNodes1, newNodes2);
    return true;

    HANDLE_ERROR:
    {
        freeBackupTreeNodes(newNodes1, newNodes2);
        return false;
    }
}

bool repairRoad(Map *map, const char *city1, const char *city2, int repairYear)
{
    return repairRoadCanCheckLength(map, city1, city2, repairYear, false, 0);
}

bool newRoute(Map *map, unsigned routeId, const char *city1, const char *city2)
{
    if (map == NULL || !isValidRouteId(routeId)
        || !isValidNameCity(city1) || !isValidNameCity(city2)
        || !areDifferentCitiesNames(city1, city2)
        || map->routes[routeId - 1] != NULL) {
        return false;
    }

    unsigned nodeNum1 = 0;
    unsigned nodeNum2 = 0;

    if (!searchInTree(map->cities, city1, &nodeNum1)
        || !searchInTree(map->cities, city2, &nodeNum2))
        return false;

    route_t *newRoute = newRouteAllVertices(map, nodeNum1, nodeNum2);

    if (newRoute == NULL)
        return false;

    map->routes[routeId - 1] = newRoute;
    return true;
}

bool newRouteVia(Map *map, unsigned routeId, const char **cities, int *years, unsigned *lengths, unsigned howManyCities)
{
    if (map == NULL || cities == NULL || years == NULL || lengths == NULL
        || howManyCities < 2 || !isValidRouteId(routeId) || map->routes[routeId - 1] != NULL)
        return false;

    if (!validRouteParams(map, cities, years, lengths, howManyCities))
        return false;

    for (unsigned i = 0; i < howManyCities - 1; ++i) {
        if (!addRoad(map, cities[i], cities[i+1], lengths[i], years[i])
            && !repairRoadCanCheckLength(map, cities[i], cities[i+1], years[i], true, lengths[i])) {
            return false;
        }
    }

    route_t *newRoute = initRoute();

    if (newRoute == NULL)
        return false;

    for (unsigned j = howManyCities - 1; j > 0; --j) {
        if (!insertToList(&(newRoute->cities), getExistingGraphVertexPtr(map->graph, cities[j], map->cities))
            || !insertToList(&(newRoute->roads), getExistingGraphEdgePtr(map->graph,
                    getExistingGraphVertexPtr(map->graph, cities[j-1], map->cities),
                    getExistingGraphVertexPtr(map->graph, cities[j], map->cities)))) {
            freeRoute(newRoute);
            return false;
        }
    }

    if (!insertToList(&(newRoute->cities), getExistingGraphVertexPtr(map->graph, cities[0], map->cities))) {
        freeRoute(newRoute);
        return false;
    }

    map->routes[routeId - 1] = newRoute;
    return true;
}

char const* getRouteDescription(Map *map, unsigned routeId)
{
    if (map == NULL)
        return NULL;

    route_t *route = NULL;
    size_t stringSize = 0;

    char *outStr = NULL;
    if (!isValidRouteId(routeId) || (route = *(map->routes + routeId - 1)) == NULL)
        outStr = (char *) mem_alloc(sizeof(char));
    else {
        stringSize = sizeOfRouteDescription(map, routeId);
        outStr = (char *) mem_alloc(sizeof(char) * (stringSize + 1));
    }

    if (outStr == NULL)
        return NULL;

    if (route == NULL) {
        *outStr = END_OF_STRING;
        return outStr;
    }

    list_t nextCity = route->cities;
    list_t nextRoad = route->roads;

    unsignedToString(outStr, digits(routeId), routeId);
    outStr += digits(routeId);
    *outStr = SEPARATE_SYMBOL;
    outStr += SEPARATE_SIZE;
    recoverNameFromTree(((graph_vertex_t *) nextCity->data)->endOfName,
                        ((graph_vertex_t *) nextCity->data)->cityNameLength,
                        outStr);

    outStr += ((graph_vertex_t *) nextCity->data)->cityNameLength;
    nextCity = nextCity->next;

    while (nextCity != NULL && nextRoad != NULL) {
        graph_vertex_t *v = nextCity->data;
        graph_edge_t *e = nextRoad->data;
        *outStr = SEPARATE_SYMBOL;
        outStr += SEPARATE_SIZE;
        unsignedToString(outStr, digits(e->length), e->length);
        outStr += digits(e->length);
        *outStr = SEPARATE_SYMBOL;
        outStr += SEPARATE_SIZE;
        signedToString(outStr, digits(e->buildYear), e->buildYear);
        outStr += digits(e->buildYear);
        *outStr = SEPARATE_SYMBOL;
        outStr += SEPARATE_SIZE;
        recoverNameFromTree(v->endOfName, v->cityNameLength, outStr);
        outStr += v->cityNameLength;
        nextCity = nextCity->next;
        nextRoad = nextRoad->next;
    }
    return (outStr - stringSize);
}

bool extendRoute(Map *map, unsigned routeId, const char *city)
{
    if (map == NULL || !isValidRouteId(routeId) || !isValidNameCity(city)
        || map->routes[routeId - 1] == NULL) {
        return false;
    }

    struct {
        bool freeVerticesMemory;
        bool extendedFirstMemory;
        bool extendedLastMemory;
        bool nextCitiesMemory;
        bool nextRoadsMemory;
    } memoryAllocated = {0, 0, 0, 0, 0};

    route_t *route = map->routes[routeId - 1];

    graph_vertex_t *first = route->cities->data;
    graph_vertex_t *last = getLastListNode(route->cities)->data;

    route_t *extendedFirst = NULL;
    route_t *extendedLast = NULL;
    route_t *bestExtendRoute = NULL;

    list_t nextCities = NULL;
    list_t nextRoads = NULL;

    bool *useVertices = (bool *) mem_alloc(
            sizeof(bool) * numberOfVertices(map->graph));

    if (useVertices == NULL) goto HANDLE_ERROR;
    else memoryAllocated.freeVerticesMemory = true;

    markNotUsedVerticesByRoute(map, routeId, useVertices);

    unsigned nodeNum = 0;

    if(!searchInTree(map->cities, city, &nodeNum)) goto HANDLE_ERROR;
    if (!useVertices[nodeNum]) goto HANDLE_ERROR;

    useVertices[getGraphVertexNumber(first)] = true;
    useVertices[getGraphVertexNumber(last)] = true;

    unsigned firstNumberOfRoutes = 0;
    uint64_t firstShortestPathLen = (uint64_t) -1;
    unsigned lastNumberOfRoutes = 0;
    uint64_t lastShortestPathLen = (uint64_t) -1;
    int firstYear = 0, lastYear = 0;

    extendedFirst = newRouteUsingVerticesSetNumberOfRoutesLengthAndYear
            (map, getGraphVertexNumber(first), nodeNum, useVertices, &firstNumberOfRoutes, &firstShortestPathLen, &firstYear);
    extendedLast = newRouteUsingVerticesSetNumberOfRoutesLengthAndYear
            (map, nodeNum, getGraphVertexNumber(last), useVertices, &lastNumberOfRoutes, &lastShortestPathLen, &lastYear);

    memoryAllocated.extendedFirstMemory = (extendedFirst != NULL);
    memoryAllocated.extendedLastMemory = (extendedLast != NULL);
    if ((firstNumberOfRoutes > 1 && lastNumberOfRoutes > 1)
        || (firstNumberOfRoutes == 1 && lastNumberOfRoutes > 1
            && (firstShortestPathLen > lastShortestPathLen  || (firstShortestPathLen == lastShortestPathLen && firstYear <= lastYear)))
        || (lastNumberOfRoutes == 1 && firstNumberOfRoutes > 1
            && (lastShortestPathLen > firstShortestPathLen  || (firstShortestPathLen == lastShortestPathLen && lastYear <= firstYear)))) {
        goto HANDLE_ERROR;
    }
    else if (extendedLast != NULL && extendedFirst == NULL) {
        bestExtendRoute = extendedLast;
    }
    else if (extendedFirst != NULL && extendedLast == NULL) {
        bestExtendRoute = extendedFirst;
    }
    else if (extendedFirst != NULL && extendedLast != NULL) {
        if (summaryLength(extendedFirst->roads) < summaryLength(extendedLast->roads)) {
            bestExtendRoute = extendedFirst;
        }
        else if (summaryLength(extendedFirst->roads) > summaryLength(extendedLast->roads)) {
            bestExtendRoute = extendedLast;
        }
        else {
            int oldestFirst = oldestRoad(map->graph, extendedFirst->cities);
            int oldestLast = oldestRoad(map->graph, extendedLast->cities);

            if (oldestFirst < oldestLast) {
                bestExtendRoute = extendedLast;
            }
            else if (oldestFirst > oldestLast) {
                bestExtendRoute = extendedFirst;
            }
            else {
                goto HANDLE_ERROR;
            }
        }
    }
    else goto HANDLE_ERROR;

    if (!copyListReversed(bestExtendRoute->cities, &nextCities)) goto HANDLE_ERROR;
    else memoryAllocated.nextCitiesMemory = true;

    if (!copyListReversed(bestExtendRoute->roads, &nextRoads)) goto HANDLE_ERROR;
    else memoryAllocated.nextRoadsMemory = true;

    if (bestExtendRoute == extendedFirst) {

        list_t lastNewCity = getOneBeforeLastListNode(nextCities);
        mem_free(lastNewCity->next); // remove last node of nextCities list
        lastNewCity->next = NULL;
        lastNewCity->next = route->cities;
        route->cities = nextCities;
        getLastListNode(nextRoads)->next = route->roads;
        route->roads = nextRoads;
    }
    else { // bestExtendedRoute == extendedLast
        list_t lastOldCity = getLastListNode(route->cities);
        removeHeadFromList(&nextCities);
        lastOldCity->next = nextCities;
        getLastListNode(route->roads)->next = nextRoads;
    }

    freeRoute(extendedFirst);
    freeRoute(extendedLast);
    mem_free(useVertices);

    return true;

    HANDLE_ERROR:
    {
        if (memoryAllocated.freeVerticesMemory) mem_free(useVertices);
        if (memoryAllocated.extendedFirstMemory) freeRoute(extendedFirst);
        if (memoryAllocated.extendedLastMemory) freeRoute(extendedLast);
        if (memoryAllocated.nextCitiesMemory) freeList(nextCities);
        if (memoryAllocated.nextRoadsMemory) freeList(nextRoads);

        return false;
    }
}

bool removeRoute(Map *map, unsigned routeId)
{
    if (map == NULL || !isValidRouteId(routeId) || map->routes[routeId - 1] == NULL)
        return false;

    freeRoute(map->routes[routeId - 1]);
    map->routes[routeId - 1] = NULL;

    return true;
}

bool removeRoad(Map *map, const char *city1, const char *city2)
{
    if (map == NULL || !isValidNameCity(city1) || !isValidNameCity(city2)
        || !areDifferentCitiesNames(city1, city2)) {
        return false;
    }

    unsigned nodeNum1 = 0;
    unsigned nodeNum2 = 0;

    if(!searchInTree(map->cities, city1, &nodeNum1)
       || !searchInTree(map->cities, city2, &nodeNum2)) {
        return false;
    }

    graph_edge_t *edge12 = NULL;
    graph_edge_t *edge21 = NULL;
    if (!isAdjacentTo(map->graph, nodeNum1, nodeNum2, &edge12)
        || !isAdjacentTo(map->graph, nodeNum2, nodeNum1, &edge21)) {
        return false;
    }

    bool *freeVertices = (bool *) mem_alloc(
            sizeof(bool) * numberOfVertices(map->graph));
    if (freeVertices == NULL)
        return false;

    graph_vertex_t *v1 = getGraphVertexPtr(map->graph, nodeNum1);
    graph_vertex_t *v2 = getGraphVertexPtr(map->graph, nodeNum2);

    unsigned howManyCommonRoutes = 0;
    uint_list_t routesNums = commonRoutesNeighboursNumbers(edge12, edge21, &howManyCommonRoutes, map);
    if (routesNums == NULL) {
        if (howManyCommonRoutes == 0) goto REMOVE_EDGE;
        mem_free(freeVertices);
        return false;
    }

    route_t **newRoutes = (route_t **) mem_alloc(
            sizeof(route_t *) * howManyCommonRoutes);
    if (newRoutes == NULL) {
        freeUIntList(routesNums);
        mem_free(freeVertices);
        return false;
    }

    uint_list_t routesNumsTemp = routesNums;
    unsigned i = 0;
    while (routesNumsTemp != NULL) {
        markNotUsedVerticesByRoute(map, routesNumsTemp->data, freeVertices);
        freeVertices[nodeNum1] = true;
        freeVertices[nodeNum2] = true;

        unsigned firstNum = firstCityInRouteNum(map->routes[routesNumsTemp->data - 1], nodeNum1, nodeNum2);
        newRoutes[i] = newRouteUsingVerticesOmitEdge(map,
                firstNum == nodeNum1 ? nodeNum1 : nodeNum2,
                firstNum == nodeNum1 ? nodeNum2 : nodeNum1,
                freeVertices, nodeNum2, nodeNum1);

        if (newRoutes[i] == NULL) {
            for (unsigned j = 0; j < i; ++j) {
                freeRoute(newRoutes[j]);
            }
            freeUIntList(routesNums);
            mem_free(freeVertices);
            mem_free(newRoutes);
            return false;
        }
        routesNumsTemp = routesNumsTemp->next;
        i += 1;
    }

    // all routes can be replaced successfully, now just replace the roads
    // to new routes
    for (unsigned k = 0; k < howManyCommonRoutes; ++k) {
        removeHeadFromList(&(newRoutes[k]->cities));
        removeLastFromList(&(newRoutes[k]->cities));
    }

    routesNumsTemp = routesNums;
    i = 0;

    while (routesNumsTemp != NULL) {
        route_t *changeRoute = map->routes[routesNumsTemp->data - 1];
        route_t *newPart = newRoutes[i];

        list_t cities = changeRoute->cities;
        list_t roads = changeRoute->roads;
        list_t lastRoad = NULL;

        while (cities->data != getGraphVertexPtr(map->graph, nodeNum1)
               && cities->data != getGraphVertexPtr(map->graph, nodeNum2)) {
            cities = cities->next;
            lastRoad = roads;
            roads = roads->next;
        }

        getLastListNode(newPart->cities)->next = cities->next;
        cities->next = newPart->cities;
        if (lastRoad != NULL)
            lastRoad->next = newPart->roads;

        getLastListNode(newPart->roads)->next = roads->next;
        if (lastRoad == NULL)
            changeRoute->roads = newPart->roads;

        mem_free(roads);
        routesNumsTemp = routesNumsTemp->next;
        i += 1;
    }

    for (unsigned j = 0; j < howManyCommonRoutes; ++j)
        mem_free(newRoutes[j]);

    mem_free(newRoutes);

    REMOVE_EDGE:
    {
        edge12 = NULL;
        edge21 = NULL;

        isAdjacentTo(map->graph, nodeNum1, nodeNum2, &edge12);
        isAdjacentTo(map->graph, nodeNum2, nodeNum1, &edge21);

        removeFromList(&(v1->edges), edge12);
        removeFromList(&(v2->edges), edge21);

        mem_free(freeVertices);
        freeUIntList(routesNums);
        mem_free(edge12);
        mem_free(edge21);

        return true;
    }
}