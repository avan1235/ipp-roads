/** @file
 * Implementation of the class keeping the single linked list
 * data structure of unsigned value in the list nodes.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "uint_list.h"

/**
 * @brief Help function in removing list nodes with values
 * @param[in] current       – current node of list considered
 * @param[in] last          – last considered node of list
 * @param[in] deleteValue   – value to be removed from list
 */
static void removeFromUIntListHelp(uint_list_t current, uint_list_t last, unsigned deleteValue)
{
    if (current != NULL) {
        if (current->data == deleteValue) {
            last->next = current->next;
            mem_free(current);
        }
        else {
            removeFromUIntListHelp(current->next, current, deleteValue);
        }
    }
}

bool insertToUIntList(uint_list_t *listPtr, unsigned value)
{
    if (listPtr != NULL) {
        uint_list_t new_head = (uint_list_t) mem_alloc(sizeof(uint_list_node_t));

        if (new_head == NULL) {
            return false;
        }
        else {
            new_head->next = (*listPtr);
            new_head->data = value;
            *listPtr = new_head;
            return true;
        }
    }
    return false;
}

void removeFromUIntList(uint_list_t *listPtr, unsigned deleteValue)
{
    if (listPtr != NULL && *listPtr != NULL) {
        if ((*listPtr)->data == deleteValue) {
            uint_list_t old = *listPtr;
            *listPtr = (*listPtr)->next;
            mem_free(old);
        }
        else {
            removeFromUIntListHelp((*listPtr)->next, *listPtr, deleteValue);
        }
    }
}

void freeUIntList(uint_list_t list)
{
    if (list != NULL) {
        uint_list_t next_node = list->next;
        while (next_node != NULL) {
            mem_free(list);
            list = next_node;
            next_node = next_node->next;
        }
        mem_free(list);
    }
}