/** @file
 * Implementation of the class keeping route data in single
 * structure.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */


#include "route.h"

route_t* initRoute(void)
{
    route_t *newRoute = (route_t *) mem_alloc(sizeof(route_t));

    if (newRoute == NULL)
        return NULL;

    newRoute->cities = NULL;
    newRoute->roads = NULL;

    return newRoute;
}

void freeRoute(route_t *route)
{
    if (route != NULL) {
        freeList(route->roads);
        freeList(route->cities);
        mem_free(route);
    }
}