/** @file
 * Interface of the class keeping all constants for input
 * properties in program.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_INPUT_PROPERTIES_H
#define IPP_ROADS_INPUT_PROPERTIES_H

/**
 * Min char out of specified chars range
 */
#define MIN_CHAR_OUT_VALUE 0

/**
 * Max char out of specified chars range
 */
#define MAX_CHAR_OUT_VALUE 31

/**
 * Minimal number that can be route ID
 */
#define MIN_ROUTE_ID 1

/**
 * Maximal number that can be route ID
 */
#define MAX_ROUTE_ID 999

/**
 * Min length of line when next line will not be read
 */
#define MIN_INPUT_LINE_LEN 1

/**
 * Symbol starting the comment line
 */
#define COMMENT_SYMBOL '#'

/**
 * Char symbol for new line in commands
 */
#define NEW_LINE_SYMBOL '\n'

/**
 * End of string symbol
 */
#define END_OF_STRING '\0'

/**
 * Junk symbol to be written to all read symbols places which are out of range
 */
#define JUNK_SYMBOL 27

/**
 * Char to separate values in route description
 */
#define SEPARATE_SYMBOL ';'

/**
 * Character code of the minimum char that is a digit
 */
#define DIGITS_START '0'

/**
 * Character code of the maximum char that is a digit
 */
#define DIGITS_END '9'

/**
 * Character code of the minimum char that is a small letter
 */
#define SMALL_LETTERS_START 'a'

/**
 * Character code of the maximum char that is a digit
 */
#define SMALL_LETTERS_END 'z'

#endif //IPP_ROADS_INPUT_PROPERTIES_H
