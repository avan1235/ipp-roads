/** @file
 * Interface of the class of country routes.
 *
 * @author Lukasz Kaminski <kamis@mimuw.edu.pl>, Marcin Peczarski <marpe@mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 20.03.2019
 */

#ifndef IPP_ROADS_MAP_H
#define IPP_ROADS_MAP_H

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "input_properties.h"
#include "graph.h"
#include "city_tree.h"
#include "route.h"

/**
 * Max number of routes in single map
 */
#define NUMBER_OF_ROUTES 999

/**
 * Number of chars of separate char in description of route
 */
#define SEPARATE_SIZE 1

/**
 * Structure of the map of routes.
 */
typedef struct Map Map;

/**
 * Map structure which is a graph of cities, array of
 * possible routes and city tree of currently names of cities.
 */
struct Map {
    graph_t *graph;     ///< Graph of cities in map
    route_t **routes;   ///< Array of possible routes in map
    tree_t cities;      ///< Tree of cities names in map
};

/** @brief Create new map structure.
 * Create new empty structure with no cities, roads and routes.
 * @return          – pointer to new created structure or NULL on
 *                    memory allocation error
 */
Map* newMap(void);

/** @brief Deletes map structure.
 * Removes the structure pointed by map. For NULL
 * pointer doesn't do anything.
 * @param[in] map        – pointer to map structure to remove
 */
void deleteMap(Map *map);

/** @brief Add new road between two different cities.
 * If any of cities doesn't exists, adds to map and then adds new road
 * between these two cities.
 * @param[in,out] map    – pointer to map structure
 * @param[in] city1      – pointer to string of one of cities
 * @param[in] city2      – pointer to string of second of cities
 * @param[in] length     – length of road
 * @param[in] builtYear  – build year of road
 * @return               – true when road was added, false when error occured:
 *                         any parameter is incorrect, cities names are the same
 *                         road between these cities already exists or
 *                         memory allocation error
 */
bool addRoad(Map *map, const char *city1, const char *city2,
             unsigned length, int builtYear);

/** @brief Change year of the last repair of road.
 * For the road between two cities change year of its last repair
 * or change this year if there was no repair.
 * @param[in,out] map    – pointer to map structure
 * @param[in] city1      – pointer to string of one of cities
 * @param[in] city2      – pointer to string of second of cities
 * @param[in] repairYear – new repair year of road
 * @return               – true when successfully changed the value, false when
 *                         parameters has wrong values, some city doesn't exist,
 *                         there is no road between two cities, given year is
 *                         earlier the old one
 */
bool repairRoad(Map *map, const char *city1, const char *city2, int repairYear);

/** @brief Connect two cities using some road.
 * Creates new route between two cities and sets the given number to it.
 * Finds the shortest path between two cities. If there are many possible
 * shortest paths, for avery option finds the oldest road in path and selects
 * the youngest option.
 * @param[in,out] map    – pointer to map structure
 * @param[in] routeId    – number of new route
 * @param[in] city1      – pointer to string of one of cities
 * @param[in] city2      – pointer to string to second of cities
 * @return               – true when route was created, false when some error
 *                         occured: any parameter is not valid, there already
 *                         exists route with specified number, any of given
 *                         cities doesn't exists or cities names are equal,
 *                         new route cannot be determined or there was memory
 *                         allocation error
 */
bool newRoute(Map *map, unsigned routeId, const char *city1, const char *city2);

/** @brief Create new route via given cities.
 * Creates new route via given cities and sets the given number to it.
 * When cities not exists in map adds them to map and then creates the route.
 * When cities has older time of build and the same lengths then actualize
 * the build years and creates the specified route via cities.
 * When some specified roads has bad length or the repair year is earlier than
 * the build year the function returns false and doesn't add following
 * cities to map but previously added cities and roads are left on map.
 * @param[in,out] map        – pointer to map structure
 * @param[in] routeId        – number of new route
 * @param[in] cities         – array of cities names to create reoute via
 * @param[in] years          – array of build/repair years of roads
 * @param[in] lengths        – array of lengths of roads
 * @param[in] howManyCities  – number of cities in new route
 * @return                   – true when route was created, false when some error
 *                             occured: any parameter is not valid, there already
 *                             exists route with specified number, some cities in
 *                             defined route are equal, years or lengths not match
 *                             the orginal in map or there was memory allocation
 *                             error
 */
bool newRouteVia(Map *map, unsigned routeId, const char **cities, int *years, unsigned *lengths, unsigned howManyCities);

/** @brief Extends specified route to som city
 * Adds to route new roads to given city to find the shortest path.
 * If there are many possible shortest paths, for avery option finds the
 * oldest road in path and selects the youngest option.
 * @param[in,out] map    – pointer to map structure
 * @param[in] routeId    – number of route
 * @param[in] city       – pointer to string of city name
 * @return               – true when route was extende, false when some error
 *                         occured: any paramater has wrong value, there is
 *                         no route of given number, there is no given city,
 *                         there is already this city in route, new route
 *                         cannot be determined or there was memory allocation
 *                         error
 */
bool extendRoute(Map *map, unsigned routeId, const char *city);

/** @brief Remove road between two different cities.
 * Remove road between two cities. If removing this route causes
 * a gap in some route then finds new route for the gap as the shortest
 * path between given cities. If there are many possible
 * shortest paths, for avery option finds the oldest road in path and selects
 * the youngest option.
 * @param[in,out] map    – pointer to map structure
 * @param[in] city1      – pointer to string of one of cities
 * @param[in] city2      – pointer to string to second of cities
 * @return               – true when road was removed, false when some error
 *                         occured: any parameter has wrong value, some city
 *                         not exist in map, there is no road between given
 *                         cities, new route cannot be determined or
 *                         there was memory allocation error
 */
bool removeRoad(Map *map, const char *city1, const char *city2);

/**
 * @ Removes route from map
 * Removes the route from map and frees the memory allocated for the
 * route components
 * @param[in,out] map   – pointer to map structure
 * @param[in] routeId   – id of route to remove
 * @return              – true when route existed in map and was removed
 *                        by the function, false when route not exists in map
 *                        or the given route id was incorrect
 */
bool removeRoute(Map *map, unsigned routeId);

/** @brief Gives the information about route
 * Returns pointer to string which has an information about route.
 * Allocate memory for this string. Returns empty string  if there is
 * no route of given id. Allocated memory has to be freed by user.
 * Format of description of route:
 * route id;city name;length of road;build year of road;city name;
 * length of road;build year of road;city name;…;city name.
 * Order of cities on list is that the cities @p city1 i @p city2, called when
 * used @ref newRoute function, where the same as in function create route
 * arguments.
 * @param[in,out] map    – pointer to amp structure
 * @param[in] routeId    – number of route
 * @return               – pointer to string or NULL when memory allocation
 *                         error occured
 */
char const* getRouteDescription(Map *map, unsigned routeId);

#endif //IPP_ROADS_MAP_H
