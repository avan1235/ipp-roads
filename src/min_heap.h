/** @file
 * Interface of the class min heap data structure
 * to be used as the priority queue.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_MIN_HEAP_H
#define IPP_ROADS_MIN_HEAP_H

#include <stdbool.h>
#include <stdlib.h>
#include <inttypes.h>

#include "memory_manager.h"

/**
 * Type of priority value in min heap.
 */
typedef uint64_t priority_t;

/**
 * Type of min heap node structure.
 */
typedef struct min_heap_node min_heap_node_t;

/**
 * Type of min heap structure.
 */
typedef struct min_heap min_heap_t;

/**
 * Min heap node structure which consists the
 * unsigned number and the priority of the node.
 */
struct min_heap_node {
    unsigned number;        ///< Number of min heap node
    priority_t priority;    ///< Priority of min heap node
};

/**
 * Min heap structure which keeps its size, array of positions of
 * the values inside the heap in nodes_array and the nodes_array which
 * is an array of all min heap nodes.
 */
struct min_heap {
    unsigned size;                  ///< Size of min heap
    unsigned *positions;            ///< Positions of nodes in array
    min_heap_node_t **nodes_array;  ///< Array Of nodes of min heap
};

/**
 * @brief Initialize new min heap node structure.
 * Create new min heap node structure and sets in fields to
 * given values. When process of allocation is not correct
 * returns NULL pointer.
 * @param[in] nodeNumber    – number of created min heap node
 * @param[in] priority      – priority of created min heap node
 * @return                  –  pointer to new min heap node
 */
min_heap_node_t* initHeapNode(unsigned nodeNumber, priority_t priority);

/**
 * @brief Init new min heap structure.
 * Create new min heap and the allocate memory of its arrays of
 * specified capacity. When encounter memory allocation problem
 * returns NULL pointer.
 * @param[in] capacity  – size of new min heap
 * @return              –  pointer to enw min heap
 */
min_heap_t *initMinHeap(unsigned capacity);

/**
 * @brief Check if is min heap empty
 * @param[in] minHeap   – min heap to be checked
 * @return              – true when empty min heap otherwise false
 */
bool isMinHeapEmpty(min_heap_t *minHeap);

/**
 * @brief Get the node with smallest priority from heap.
 * Extract specified node and heapify the structure to keep
 * the property of min heap. Extracted node has to be freed
 * manually outside the function of extracting.
 * @param[in] minHeap       – min heap from which extract min priority node
 * @return                  – pointer to min priority node from heap. When
 *                            heap empty returns NULL pointer
 */
min_heap_node_t *extractMinNodeFromHeap(min_heap_t *minHeap);

/**
 * @brief Decrease the key of min heap node.
 * Change the value of priority of specified node in min heap.
 * @param[in] minHeap       – min heap to be changed
 * @param[in] number        – position in array of decreased value
 * @param[in] priority      –  new priority of min heap node
 */
void decreaseKey(min_heap_t *minHeap, unsigned number, priority_t priority);

/**
 * @brief Check if some value is still in min heap.
 * @param[in] minHeap       – min heap to search for value
 * @param[in] number        – index of value in min heap
 * @return                  – true when in min heap otherwise false
 */
bool isInMinHeap(min_heap_t *minHeap, unsigned number);

/**
 * @brief Free all min heap fields.
 * Min heap fields are freed and the nodes in min heap which
 * are still in it are freed but the nodes removed before has
 * to be freed manually by user.
 * @param[in] minHeap       – min hep to be freed
 */
void freeMinHeap(min_heap_t *minHeap);

#endif //IPP_ROADS_MIN_HEAP_H
