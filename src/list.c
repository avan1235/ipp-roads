/** @file
 * Implementation of the class keeping the single linked list
 * data structure of pointers in the list nodes.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "list.h"

/**
 * @brief Help function in removing list node
 * @param[in] current       – current node of list considered
 * @param[in] last          – last considered node of list
 * @param[in] deleteData    – pointer to be removed from list
 */
static void removeFromListHelp(list_t current, list_t last, void *deleteData)
{
    if (current != NULL) {
        if (current->data == deleteData) {
            last->next = current->next;
            mem_free(current);
        }
        else {
            removeFromListHelp(current->next, current, deleteData);
        }
    }
}

bool insertToList(list_t *listPtr, void *newData)
{
    if (listPtr != NULL) {
        list_t new_head = (list_t) mem_alloc(sizeof(list_node_t));

        if (new_head == NULL) {
            return false;
        }
        else {
            new_head->data = newData;
            new_head->next = (*listPtr);
            (*listPtr) = new_head;
            return true;
        }
    }
    return false;
}

void removeFromList(list_t *listPtr, void *deleteData)
{
    if (listPtr != NULL && *listPtr != NULL) {
        if ((*listPtr)->data == deleteData) {
            list_t old = *listPtr;
            *listPtr = (*listPtr)->next;
            mem_free(old);
        }
        else {
            removeFromListHelp((*listPtr)->next, *listPtr, deleteData);
        }
    }
}

void removeHeadFromList(list_t *listPtr)
{
    if (listPtr != NULL && *listPtr != NULL) {
        list_t nextNode = (*listPtr)->next;
        mem_free(*listPtr);
        *listPtr = nextNode;
    }
}

void removeLastFromList(list_t *listPtr)
{
    if (listPtr != NULL && *listPtr != NULL) {
        if ((*listPtr)->next == NULL) {
            mem_free(*listPtr);
            *listPtr = NULL;
            return;
        }
        list_t notLast = getOneBeforeLastListNode(*listPtr);
        mem_free(notLast->next);
        notLast->next = NULL;
    }
}

bool copyList(list_t from, list_t *toPtr)
{
    *toPtr = NULL;

    if (from == NULL)
        return true;

    list_t newNode = (list_t) mem_alloc(sizeof(list_node_t));

    if (newNode == NULL) {
        *toPtr = NULL;
        return false;
    }

    *toPtr = newNode;
    newNode->data = from->data;

    list_t lastNode = newNode;
    from = from->next;

    while (from != NULL) {
        list_t newNodeInsert = (list_t) mem_alloc(sizeof(list_node_t));

        if (newNodeInsert == NULL) {
            freeList(*toPtr);
            *toPtr = NULL;
            return false;
        }

        lastNode->next = newNodeInsert;
        newNodeInsert->data = from->data;
        lastNode = newNodeInsert;

        from = from->next;
    }

    lastNode->next = NULL;
    return true;
}

bool copyListReversed(list_t from, list_t *toPtr)
{
    *toPtr = NULL;

    if (from == NULL) {
        return true;
    }

    list_t list = (list_t) mem_alloc(sizeof(list_node_t));

    if (list == NULL) {
        return false;
    }

    list->data = from->data;
    list->next = NULL;

    from = from->next;

    while (from != NULL) {
        list_t  newNode = (list_t) mem_alloc(sizeof(list_node_t));
        if (newNode == NULL) {
            freeList(list);
            return false;
        }

        newNode->data = from->data;
        newNode->next = list;
        list = newNode;

        from = from->next;
    }

    *toPtr = list;

    return true;
}

list_t getLastListNode(list_t list)
{
    if (list != NULL) {
        while (list->next != NULL) {
            list = list->next;
        }
        return list;
    }
    return NULL;
}

list_t getOneBeforeLastListNode(list_t list)
{
    if (list != NULL && list->next != NULL) {
        while (list->next->next != NULL) {
            list = list->next;
        }
        return list;
    }
    return NULL;
}

bool existsInList(list_t list, void *data1, void *data2)
{
    while (list != NULL) {
        if (list->data == data1 || list->data == data2)
            return true;
        list = list->next;
    }
    return false;
}

void freeListWithContent(list_t list, void (*freeNodeFun)(void *))
{
    if (list != NULL) {
        list_t next_node = list->next;
        while (next_node != NULL) {
            freeNodeFun(list->data);
            mem_free(list);
            list = next_node;
            next_node = next_node->next;
        }
        freeNodeFun(list->data);
        mem_free(list);
    }
}

void freeList(list_t list)
{
    if (list != NULL) {
        list_t next_node = list->next;
        while (next_node != NULL) {
            mem_free(list);
            list = next_node;
            next_node = next_node->next;
        }
        mem_free(list);
    }
}