/** @file
 * Implementation of the class which can change the way
 * of memory allocation in whole project by just replacing
 * the standard methods of memory allocation. When using original
 * methods of memory allocation in project it can be easily achieved
 * to add some code to memory allocation functions.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "memory_manager.h"

/**
 * Function which is defined only when wrapping for malloc enabled in gcc
 * otherwise it is NULL function
 * @param[in] size      – size of memory to allocate
 * @return              – pointer to new allocated memory
 */
void* __real_malloc(size_t size) __attribute__((weak));

/**
 * Function which is defined only when wrapping for realloc enabled in gcc
 * otherwise it is NULL function
 * @param[in] oldMem       – pointer to old allocated memory to be reallocated
 * @param[in] newSize      – size of memory to reallocate
 * @return                 – pointer to new allocated memory
 */
void* __real_realloc(void *oldMem, size_t newSize) __attribute__((weak));

void* mem_alloc(size_t size)
{
    if(__real_malloc)
        return __real_malloc(size);

    return malloc(size);
}

void* mem_realloc(void *oldMem, size_t newSize)
{
    if(__real_realloc)
        return __real_realloc(oldMem, newSize);

    return realloc(oldMem, newSize);
}

void mem_free(void *mem)
{
    if (mem != NULL)
        free(mem);
}