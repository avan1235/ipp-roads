/** @file
 * Implementation of the class parsing the data from string data line.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "parser.h"

/**
 * @brief Compare two strings.
 * When strings are equal return true, otherwise return false
 * @param[in] s1            – first string to be checked
 * @param[in] s2            – second string to be checked
 * @return                  –  true when strings are equal otherwise false
 */
static bool equalStrings(const char *s1, const char *s2)
{
    return strcmp(s1, s2) == 0;
}

/**
 * @brief Compare two positive numbers represented by strings.
 * Compare the lengths of given strings and when are equal the compare
 * the digits of strings. Works with valid strings numbers.
 * @param[in] num1          – first string of number
 * @param[in] num2          – second string of number
 * @return                  – 1 when first number is greater, -1 when first is
 *                            smaller and 0 when given numbers are equal
 */
static int compareStringPositiveNumbers(const char *num1, const char *num2)
{
    const char *newNum1 = num1;
    const char *newNum2 = num2;

    while (*newNum1 == '0')
        newNum1 += 1;
    if (*newNum1 == END_OF_STRING && *num1 != END_OF_STRING)
        num1 = newNum1 - 1;
    else
        num1 = newNum1;

    while (*newNum2 == '0')
        newNum2 += 1;
    if (*newNum2 == END_OF_STRING && *num2 != END_OF_STRING)
        num2 = newNum2 - 1;
    else
        num2 = newNum2;

    if (strlen(num1) > strlen(num2)) return 1;
    else if (strlen(num1) < strlen(num2)) return -1;
    else return strcmp(num1, num2);
}

/**
 * @brief Compare two numbers represented by strings.
 * Compare the numbers represented by validated string which can be positive
 * or integer numbers. Uses comparing of positive numbers.
 * @param[in] num1          – first string of number
 * @param[in] num2          – second string of number
 * @return                  – 1 when first number is greater, -1 when first is
 *                            smaller and 0 when given numbers are equal
 */
static int compareStringNumbers(const char  *num1, const char *num2)
{
    if (*num1 == MINUS && *num2 == MINUS)
        return compareStringPositiveNumbers(num2 + 1, num1 + 1);
    else if (*num1 == MINUS)
        return -1;
    else if (*num2 == MINUS)
        return 1;
    else
        return compareStringPositiveNumbers(num1, num2);
}

/**
 * @brief Check if given char is a digit representing char
 * @param[in] num           – char of digit to be checked
 * @return                  – true when has digit char otherwise false
 */
static bool isDigit(char num)
{
    return num >= '0' && num <= '9';
}

/**
 * @brief Check if given string is valid int representing string.
 * Check if string consists with only digits or MINUS character.
 * Then compare its value with max int and min int strings values.
 * @param[in] intString     – number string to be checked
 * @return                  – true when valid string otherwise false
 */
static bool isValidInt(const char *intString)
{
    if (intString == NULL)
        return false;

    const char *tempPtr = intString;
    bool valid;
    if (*tempPtr == MINUS) {
        tempPtr += 1;
        valid = strlen(intString) > 1;
    }
    else
        valid = strlen(intString) > 0;

    while (*tempPtr != END_OF_STRING) {
        if (!isDigit(*tempPtr)) return false;
        tempPtr += 1;
    }

    return valid
            && compareStringNumbers(intString, MAX_INT_STRING) <= 0
            && compareStringNumbers(intString, MIN_INT_STRING) >= 0;
}

/**
 * @brief Check if given string is valid unsigned int representing string.
 * Check if string consists with only digits.
 * Then compare its value with max int and min unsigned strings values.
 * @param[in] uintString     – number string to be checked
 * @return                   – true when valid string otherwise false
 */
static bool isValidUnsigned(const char *uintString)
{
    if (uintString == NULL)
        return false;

    const char *tempPtr = uintString;
    while (*tempPtr != END_OF_STRING) {
        if (!isDigit(*tempPtr)) return false;
        tempPtr += 1;
    }
    return strlen(uintString) > 0
            && compareStringNumbers(uintString, MAX_UINT_STRING) <= 0;
}

/**
 * @brief Convert string to int number.
 * Convert a valid number string into the its int value.
 * @param[in] string        – string to be parsed to number value
 * @return                  – calculated value of number
 */
static int stringToInt(const char *string)
{
    int result = 0;
    bool negative = ((*string) == MINUS);
    string += negative ? 1 : 0;
    while (*string != END_OF_STRING) {
        result *= 10;
        result += (*string) - '0';
        string += 1;
    }
    return negative ? -result : result;
}

/**
 * @brief Convert string to unsigned number.
 * Convert a valid number string into the its unsigned value.
 * @param[in] string        – string to be parsed to number value
 * @return                  – calculated value of number
 */
static unsigned stringToUnsigned(const char *string)
{
    unsigned result = 0;
    while (*string != END_OF_STRING) {
        result *= 10;
        result += (*string) - '0';
        string += 1;
    }
    return result;
}

/**
 * @brief Count chars in string.
 * Count specified chars in given string.
 * @param[in] line           – string to search for char
 * @param[in] c              – char to be found and counted
 * @return                   – number of occurences of c in line
 */
static unsigned countChars(const char *line, char c)
{
    unsigned result = 0;
    while (*line != END_OF_STRING) {
        if (*line == c)
            result += 1;
        line += 1;
    }
    return result;
}

/**
 * @brief Get command type based on command name.
 * @param[in] command       – string with the command
 * @return                  – integer representation of command. Whe command
 *                            not recognized then returns ERROR_COMMAND
 */
static unsigned getCommandType(const char *command)
{
    if (equalStrings(command, ADD_ROAD_COMMAND))
        return ADD_ROAD;
    else if (equalStrings(command, REPAIR_ROAD_COMMAND))
        return REPAIR_ROAD;
    else if (equalStrings(command, GET_ROUTE_DESCRIPTION_COMMAND))
        return GET_ROUTE_DESCRIPTION;
    else if (equalStrings(command, NEW_ROUTE_COMMAND))
        return NEW_ROUTE;
    else if (equalStrings(command, EXTEND_ROUTE_COMMAND))
        return EXTEND_ROUTE;
    else if (equalStrings(command, REMOVE_ROAD_COMMAND))
        return REMOVE_ROAD;
    else if (equalStrings(command, REMOVE_ROUTE_COMMAND))
        return REMOVE_ROUTE;
    else if (isValidUnsigned(command)) // TODO test this case changed
        return NEW_ROUTE_VIA;
    else
        return ERROR_COMMAND;
}

/**
 * @brief Validate city name from input string
 * Checks if city name is non empty string so it can be passed to
 * function using the city name and then validated for existing
 * special chars from defined range
 * @param[in] city          – name of city to validate
 * @return                  – true when name of city valid, otherwise false
 */
static bool isNotEmptyCityName(const char *city)
{
    if (strlen(city) < 1)
        return false;

    return true;
}

/**
 * @brief Initialize new command structure fields.
 * Set the defined command type to command and initialize the array of its
 * arguments of the specified size.
 * @param[in] command       – pointer to command to initialize
 * @param[in] commandType   – type of command to initialize
 * @param[in] numberOfArgs  – number of arguments of initialized command
 * @return                  – true when initialized successfully, otherwise
 *                            false
 */
static bool initCommand(command_t *command, unsigned commandType, unsigned numberOfArgs)
{
    command->commandType = commandType;
    command->args = (arg_t *) mem_alloc(numberOfArgs * sizeof(arg_t));
    return command->args != NULL;
}

/**
 * @brief Validate many cities params.
 * Validate cities names params in an array of params where cities names
 * should be on special positions of array.
 * @param[in] parts         – array of strings to be checked
 * @param[in] numberOfParts – number of elements in strings array
 * @return                  – true when all cities names valid otherwise false
 */
static bool validateCitiesParams(const char **parts, unsigned numberOfParts)
{
    for (unsigned i = 0; i < numberOfParts; ++i) {
        if (i % 3 == 1 && !isNotEmptyCityName(parts[i]))
            return false;
    }
    return true;
}

/**
 * @brief Validate many int params.
 * Validate ints values in an array of params where ints should be on
 * special positions of array.
 * @param[in] parts         – array of strings to be checked
 * @param[in] numberOfParts – number of elements in strings array
 * @return                  – true when all ints are valid otherwise false
 */
static bool validateIntsParams(const char **parts, unsigned numberOfParts)
{
    for (unsigned i = 1; i < numberOfParts; ++i) {
        if (i % 3 == 0 && !isValidInt(parts[i]))
            return false;
    }
    return true;
}

/**
 * @brief Validate many unsigned params.
 * Validate unsigned values in an array of params where unsigneds should be on
 * special positions of array.
 * @param[in] parts         – array of strings to be checked
 * @param[in] numberOfParts – number of elements in strings array
 * @return                  – true when all unsigned are valid otherwise false
 */
static bool validateUnsignedParams(const char **parts, unsigned numberOfParts)
{
    for (unsigned i = 1; i < numberOfParts; ++i) {
        if (i % 3 == 2 && !isValidUnsigned(parts[i]))
            return false;
    }
    return isValidUnsigned(parts[0]);
}

bool parseData(char *line, command_t *command, bool *hasCommand, bool *inputError)
{
    const char **parts = NULL;

    if (*line != NEW_LINE_SYMBOL && *line != COMMENT_SYMBOL) {
        unsigned numberOfParts = countChars(line, SEPARATE_SYMBOL) + 1;
        parts = (const char **) mem_alloc(sizeof(const char *) * numberOfParts);
        if (parts == NULL)
            goto MEMORY_ERROR;
        parts[0] = line;
        unsigned currentPart = 1;
        while (*line != END_OF_STRING) {
            if (*line == SEPARATE_SYMBOL) {
                parts[currentPart] = line + 1;
                *line = END_OF_STRING;
                currentPart += 1;
            }
            line += 1;
        }

        switch (getCommandType(parts[0]))
        {
            case ADD_ROAD:
            {
                if (numberOfParts == ADD_ROAD_ARGS + 1
                    && isNotEmptyCityName(parts[1])
                    && isNotEmptyCityName(parts[2])
                    && isValidUnsigned(parts[3])
                    && isValidInt(parts[4]))
                {
                    if (!initCommand(command, ADD_ROAD, ADD_ROAD_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].city = parts[1];
                    (command->args)[1].city = parts[2];
                    (command->args)[2].length = stringToUnsigned(parts[3]);
                    (command->args)[3].year = stringToInt(parts[4]);
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case REPAIR_ROAD:
            {
                if (numberOfParts == REPAIR_ROAD_ARGS + 1
                    && isNotEmptyCityName(parts[1])
                    && isNotEmptyCityName(parts[2])
                    && isValidInt(parts[3]))
                {
                    if (!initCommand(command,  REPAIR_ROAD, REPAIR_ROAD_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].city = parts[1];
                    (command->args)[1].city = parts[2];
                    (command->args)[2].year = stringToInt(parts[3]);
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case GET_ROUTE_DESCRIPTION:
            {
                if (numberOfParts == GET_ROUTE_DESCRIPTION_ARGS + 1
                    && isValidUnsigned(parts[1]))
                {
                    if (!initCommand(command,  GET_ROUTE_DESCRIPTION, GET_ROUTE_DESCRIPTION_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].id = stringToUnsigned(parts[1]);
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case NEW_ROUTE:
            {
                if (numberOfParts == NEW_ROUTE_ARGS + 1
                    && isValidUnsigned(parts[1])
                    && isNotEmptyCityName(parts[2])
                    && isNotEmptyCityName(parts[3]))
                {
                    if (!initCommand(command, NEW_ROUTE, NEW_ROUTE_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].id = stringToUnsigned(parts[1]);
                    (command->args)[1].city = parts[2];
                    (command->args)[2].city = parts[3];
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case EXTEND_ROUTE:
            {
                if (numberOfParts == EXTEND_ROUTE_ARGS + 1
                    && isValidUnsigned(parts[1])
                    && isNotEmptyCityName(parts[2]))
                {
                    if (!initCommand(command, EXTEND_ROUTE, EXTEND_ROUTE_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].id = stringToUnsigned(parts[1]);
                    (command->args)[1].city = parts[2];
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case REMOVE_ROAD:
            {
                if (numberOfParts == REMOVE_ROAD_ARGS + 1
                    && isNotEmptyCityName(parts[1])
                    && isNotEmptyCityName(parts[2]))
                {
                    if (!initCommand(command, REMOVE_ROAD, REMOVE_ROAD_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].city = parts[1];
                    (command->args)[1].city = parts[2];
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case REMOVE_ROUTE:
            {
                if (numberOfParts == REMOVE_ROUTE_ARGS + 1
                    && isValidUnsigned(parts[1]))
                {
                    if (!initCommand(command, REMOVE_ROUTE, REMOVE_ROUTE_ARGS))
                        goto MEMORY_ERROR;
                    (command->args)[0].id = stringToUnsigned(parts[1]);
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            case NEW_ROUTE_VIA:
            {
                if (numberOfParts % 3 == 2 && numberOfParts > 2
                    && validateCitiesParams(parts, numberOfParts)
                    && validateIntsParams(parts, numberOfParts)
                    && validateUnsignedParams(parts, numberOfParts))
                {
                    if (!initCommand(command,  NEW_ROUTE_VIA, NEW_ROUTE_VIA_ARGS))
                        goto MEMORY_ERROR;

                    (command->args)[0].id = stringToUnsigned(parts[0]);
                    (command->args)[4].numberOfCities = ((numberOfParts + 1) / 3);
                    (command->args)[1].cities = (const char **) mem_alloc(
                            sizeof(const char *) *
                            (command->args)[4].numberOfCities);
                    (command->args)[2].years = (int *) mem_alloc(sizeof(int) *
                                                                 ((command->args)[4].numberOfCities -
                                                                  1));
                    (command->args)[3].lengths = (unsigned *) mem_alloc(
                            sizeof(unsigned) *
                            ((command->args)[4].numberOfCities - 1));

                    if ((command->args)[1].cities == NULL
                        || (command->args)[2].years == NULL
                        || (command->args)[3].lengths == NULL) {
                        mem_free((command->args)[1].cities);
                        mem_free((command->args)[2].years);
                        mem_free((command->args)[3].lengths);
                        mem_free(command->args);
                        goto MEMORY_ERROR;
                    }
                    for (unsigned i = 1; i < numberOfParts; ++i) {
                        if (i % 3 == 1)
                            ((command->args)[1].cities)[i/3]= parts[i];
                        else if (i % 3 == 2)
                            ((command->args)[3].lengths)[i/3] = stringToUnsigned(parts[i]);
                        else // if (i % 3 == 0)
                            ((command->args)[2].years)[i/3-1] = stringToInt(parts[i]);
                    }
                    goto SUCCESS;
                }
                else goto ERROR_INPUT;
            } break;
            default: // case ERROR_COMMAND:
            {
                goto ERROR_INPUT;
            }
        }
    }
    else goto EMPTY_INPUT;

    SUCCESS:
    {
        *hasCommand = true;
        *inputError = false;
        mem_free(parts);
        return true;
    }

    ERROR_INPUT:
    {
        *hasCommand = false;
        *inputError = true;
        mem_free(parts);
        return true;
    }

    EMPTY_INPUT:
    {
        *hasCommand = false;
        *inputError = false;
        return true;
    }

    MEMORY_ERROR:
    {
        *hasCommand = false;
        *inputError = false;
        mem_free(parts);
        return false;
    }
}