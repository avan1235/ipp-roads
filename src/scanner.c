/** @file
 * Implementation of the class reading the data from standard input.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "scanner.h"

/**
 * @brief Check if int character is out of good chars range
 * @param[in] c             – char to be checked
 * @return                  – true when is out of good characters range
 *                            otherwise false
 */
static inline bool isCharOutOfRange(int c)
{
    return (c >= MIN_CHAR_OUT_VALUE && c <= MAX_CHAR_OUT_VALUE);
}

/**
 * @brief Check if line is empty line
 * Checks if the line consists only the new line character and no other chars
 * @param lastChar          – last char read in line
 * @param inputLen          – number of input chars when reading line
 * @return                  – true when line was empty otherwise false
 */
static inline bool isEmptyLine(int lastChar, uint64_t inputLen)
{
    return (lastChar == NEW_LINE_SYMBOL && inputLen == 0);
}

/**
 * @brief Check if line can be a correct input command
 * Checks if lines ends with new line symbol or is an empty line
 * @param lastChar          – last char read in line
 * @param inputLen          – number of input chars when reading line
 * @return                  – true when line was can be a full line command otherwise false
 */
static inline bool isFullLineCommand(int lastChar, uint64_t inputLen)
{
    return (lastChar == NEW_LINE_SYMBOL || inputLen == 0);
}

/**
 * @brief Check if char can start the correct new line of input
 * Check if given char can be the first one in new input line in order
 * to optimize the input by not saving the unused data when sure that
 * the current line is not the correct one
 * @param c                 – character to be checked
 * @return                  – true when valid char to begin line otherwise false
 */
static inline bool canCharStartCorrectLine(char c)
{
    return (c >= DIGITS_START && c <= DIGITS_END)
            || (c >= SMALL_LETTERS_START && c <= SMALL_LETTERS_END);
}

/**
 * @brief Read the input line and not save
 * Read the input line and not save it when it is incorrect so the data
 * included in given line would not be correctly used. Reads till NEW_LINE_SYMBOL
 * or end of input file when last line does not end with NEW_LINE_SYMBOL
 * @return                  – last value read from input
 */
static inline int readTillEndOfLine()
{
    int input;
    while ((input = fgetc(stdin)) != EOF && input != NEW_LINE_SYMBOL) {}
    return input;
}

uint64_t getInputLine(char **linePtr)
{
    int tempInput;
    char tempChar;
    char *tempCharPtr = NULL;

    uint64_t len = 0;
    uint64_t curLineSize = INIT_LINE_LEN;

    tempCharPtr = (char *) mem_alloc(curLineSize * sizeof(char));

    if (tempCharPtr == NULL) {
        mem_free(*linePtr);
        *linePtr = NULL;
        return MIN_INPUT_LINE_LEN - 1;
    }
    else
        (*linePtr) = tempCharPtr;

    while ((tempInput = fgetc(stdin)) != EOF && tempInput != NEW_LINE_SYMBOL)
    {
        if (!isCharOutOfRange(tempInput))
            tempChar = (char) tempInput;
        else
            tempChar = (char) JUNK_SYMBOL;

        (*linePtr)[len] = tempChar;
        ++len;

        if (len == 1 && !canCharStartCorrectLine(tempChar)) {
            tempInput = readTillEndOfLine();
            break;
        }

        if (len > curLineSize - 1) {
            curLineSize *= LINE_GROWTH_FACTOR;
            tempCharPtr = (char *) mem_realloc(*linePtr, curLineSize * sizeof(char));

            if (tempCharPtr == NULL) {
                mem_free(*linePtr);
                *linePtr = NULL;
                return MIN_INPUT_LINE_LEN - 1;
            }
            else {
                (*linePtr) = tempCharPtr;
            }
        }
    }

    // SPECIAL CASES FOR INPUT CHARS

    // empty line with only NEW_LINE_SYMBOL gets the NEW_LINE_SYMBOL
    // in string to be parsed as the empty input - string starting
    // with NEW_LINE_SYMBOL (not to be parsed as the last of the all inputs
    // given in input source) the goal is to catch the difference
    // between EOF and NEW_LINE_SYMBOL
    if (isEmptyLine(tempInput, len)) {
        (*linePtr)[len] = NEW_LINE_SYMBOL;
        ++len;
    }

    // when the read input does not ends with NEW_LINE_SYMBOL so it is
    // not a fully correct line of command then it has to be changed
    // into something what will be treated as a junk by the parser
    // so we convert its last char (which we know that exists)
    // into the JUNK_SYMBOL to force parser to treat this line
    // as unusable for interpreting
    if (!isFullLineCommand(tempInput, len))
        (*linePtr)[len-1] = JUNK_SYMBOL;

    (*linePtr)[len] = END_OF_STRING;

    return len;
}
