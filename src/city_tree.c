/** @file
 * Implementation of the class keeping all cities in ternary
 * tree data structure.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "city_tree.h"

/**
 * @brief Add new nodes to tree of cities.
 * Recursive function for inserting nodes to ternary tree and remember
 * pointers to created nodes in order to be able to remove them if allocation
 * memory at some point will not be possible and will have to restore
 * the data structure to the old state.
 * @param[in] currentTree           – tree to insert new city name
 * @param[in] city                  – city name to insert to tree
 * @param[in] parentTree            – parent tree of new created node
 * @param[in] nodeNumber            – inserted node number to be connected with
 *                                  – given city name
 * @param[in,out] lastPtr           – after success allocation of all tree
 *                                  – nodes it will point to leaf of created tree
 * @param[in,out] newNodes          – array of pointers to new allocated nodes of tree
 * @param[in,out] numberOfInserted  – number of new created nodes of tree
 * @return                          – new created node of tree
 */
static tree_t insertRecursive(tree_t currentTree, const char *city, tree_t parentTree,
                              unsigned nodeNumber, tree_node_t **lastPtr, tree_t *newNodes,
                              unsigned *numberOfInserted)
{
    if (currentTree == NULL) {
        currentTree = (tree_t) mem_alloc(sizeof(tree_node_t));

        if (currentTree == NULL) {
            return NULL;
        }
        else {
            // add newly allocated pointers to
            // the previously allocated array of
            // new pointer to be able to recover
            // te tree before inserting operation
            *(newNodes + (*numberOfInserted)) = currentTree;
            (*numberOfInserted) += 1;
        }
        currentTree->splitChar = *city;
        currentTree->parent = parentTree;
        currentTree->lower = NULL;
        currentTree->next.son = NULL;
        currentTree->higher = NULL;
    }

    if (*city < currentTree->splitChar) {
        tree_t tempTree = insertRecursive(currentTree->lower, city, currentTree,
                                          nodeNumber, lastPtr, newNodes,
                                          numberOfInserted);
        if (tempTree != NULL)
            currentTree->lower = tempTree;
    }
    else if (*city > currentTree->splitChar) {
        tree_t tempTree = insertRecursive(currentTree->higher, city, currentTree,
                                          nodeNumber, lastPtr, newNodes,
                                          numberOfInserted);
        if (tempTree != NULL)
            currentTree->higher = tempTree;
    } // if (*city == currentTree->splitChar)
    else if (*city != END_OF_STRING) {
        tree_t tempTree = insertRecursive(currentTree->next.son, city + 1, currentTree,
                                          nodeNumber, lastPtr, newNodes,
                                          numberOfInserted);
        if (tempTree != NULL)
            currentTree->next.son = tempTree;
    }
    else { // if (*city == END_OF_STRING)
        currentTree->next.graph_node_number = nodeNumber;
        *lastPtr = currentTree;
    }
    return currentTree;
}

/**
 * @brief Recover not null city name from tree.
 * Recover the characters of the city name inserted to tree in
 * a way by going from the bottom of the tree and setting
 * the characters in *strPtr to read from nodes characters.
 * When this function is called we have to be sure that the
 * string exists so before calling the function user have
 * to check the given condition.
 * @param[in] curTree           – currently considered tree node
 * @param[in] lastTree          – last considered tree node
 *                                when start function set to NULL
 * @param[in] len               – length of the not recovered string
 * @param[in,out] stringPtr     - pointer to char where next character
 *                                will be saved after recovering process
 */
static void recoverNotNullName(tree_t curTree, tree_t lastTree, size_t len, char *stringPtr)
{
    while (len > 0 && curTree != NULL) {
        if (curTree->next.son == lastTree) {
            *(stringPtr + len - 1) = curTree->splitChar;
            len -= 1;
        }
        lastTree = curTree;
        curTree = curTree->parent;
    }
}

/**
 * @brief Clean the structure help function.
 * Remove all the nodes of city tree in a recursive way.
 * Uses the fact that END_OF_STRING character is inserted
 * to tree always on left because has ASCII code equal to 0
 * so has to be treated specially
 * @param tree          – tree to be freed
 */
static void freeTreeHelp(tree_t tree)
{
    if (tree != NULL) {
        if (tree->splitChar != END_OF_STRING) {
            if (tree->next.son != NULL) {
                freeTreeHelp(tree->next.son);
                mem_free(tree->next.son);
            }
            if (tree->lower != NULL) {
                freeTreeHelp(tree->lower);
                mem_free(tree->lower);
            }
        }
        if (tree->higher != NULL) {
            freeTreeHelp(tree->higher);
            mem_free(tree->higher);
        }
    }
}

bool insertToTree(tree_t *treePtr, const char *city, unsigned nodeNumber,
                  tree_t *leafPtr, tree_t *newNodes, unsigned *numberOfInsertedNodes)
{
    if (treePtr == NULL)
        return false;

    tree_t insertResult = insertRecursive(*treePtr, city, NULL, nodeNumber,
                                          leafPtr, newNodes,
                                          numberOfInsertedNodes);
    if (insertResult == NULL) {
        return false;
    }
    else {
        *treePtr = insertResult;
        return true;
    }
}

bool searchInTree(tree_t tree, const char *city, unsigned *nodeNumber)
{
    while (tree != NULL)
    {
        if (*city < tree->splitChar) {
            tree = tree->lower;
        }
        else if (*city > tree->splitChar) {
            tree = tree->higher;
        }
        else { // if (*city == tree->splitChar)
            if (*city == END_OF_STRING) {
                *nodeNumber = tree->next.graph_node_number;
                return true;
            }
            else {
                tree = tree->next.son;
            }
            city += 1;
        }
    }
    return false;
}

void recoverNameFromTree(tree_t leaf, size_t len, char *stringPtr)
{
    if (leaf != NULL && leaf->splitChar == END_OF_STRING) {
        *(stringPtr + len) = END_OF_STRING;
        recoverNotNullName(leaf->parent, leaf, len, stringPtr);
    }
}

void freeTree(tree_t tree)
{
    freeTreeHelp(tree);
    mem_free(tree);
}