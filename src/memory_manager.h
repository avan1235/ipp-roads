/** @file
 * Interface of the class which can change the way
 * of memory allocation in whole project by just replacing
 * the standard methods of memory allocation. When using original
 * methods of memory allocation in project it can be easily achieved
 * to add some code to memory allocation functions.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_MEMORY_MANAGER_H
#define IPP_ROADS_MEMORY_MANAGER_H

#include <stdlib.h>

/**
 * @brief Memory allocation wrapper
 * Allocate the memory and return the pointer to the allocated
 * memory. When problem in allocation occured returns NULL pointer
 * @param[in] size      – size of memory to allocate in bytes
 * @return              – pointer to allocated memory
 */
void* mem_alloc(size_t size);

/**
 * @brief Memory reallocation wrapper
 * Allocate the new block of memory of the bigger size than the old one
 * and reallocate all data from the old memory block to the new place.
 * @param[in] oldMem    – size of memory to reallocate
 * @param[in] newSize   – size of new allocated memory block
 * @return              – pointer to new allocated
 */
void* mem_realloc(void *oldMem, size_t newSize);

/**
 * @brief Free allocated memory wrapper
 * @param[in] mem       – pointer to memory to be freed
 */
void mem_free(void *mem);

#endif //IPP_ROADS_MEMORY_MANAGER_H
