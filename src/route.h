/** @file
 * Interface of the class keeping route data in single
 * structure.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_ROUTE_H
#define IPP_ROADS_ROUTE_H

#include <stdlib.h>

#include "list.h"
#include "memory_manager.h"

/**
 * Type of route structure.
 */
typedef struct route route_t;

/**
 * Route structure which keep the list of pointers
 * to cities vertices in certain route and a list
 * of pointer to edges in graph in this route.
 */
struct route {
    list_t cities;              ///< List of cities vertices in route
    list_t roads;               ///< List of edges (roads) in route
};

/**
 * @brief Init new route structure.
 * Initialize new route structure and set it fields
 * to NULL pointer (empty lists)
 * @return      – pointer to new route structure. When memory
 *                allocation error occured returns NULL pointer.
 */
route_t* initRoute(void);

/**
 * @brief Free route structure.
 * Free route structure by freeing the lists from which it
 * consists of at the structure of route.
 * @param[in] route     – route to be freed
 */
void freeRoute(route_t *route);

#endif //IPP_ROADS_ROUTE_H
