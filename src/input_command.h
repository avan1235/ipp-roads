/** @file
 * Interface of the class representing the input command from for parser.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_INPUT_COMMAND_H
#define IPP_ROADS_INPUT_COMMAND_H

/**
 * Not valid command type specified number
 */
#define ERROR_COMMAND 0

/**
 * Add road command specified number
 */
#define ADD_ROAD 1

/**
 * Number of args of add road command
 */
#define ADD_ROAD_ARGS 4

/**
 * Repair road command specified number
 */
#define REPAIR_ROAD 2

/**
 * Number of args of repair road command
 */
#define REPAIR_ROAD_ARGS 3

/**
 * Get route description command specified number
 */
#define GET_ROUTE_DESCRIPTION 3

/**
 * Number of args of get route description command
 */
#define GET_ROUTE_DESCRIPTION_ARGS 1

/**
 * New route via command specified number
 */
#define NEW_ROUTE_VIA 4

/**
 * Number of args of new route via command
 */
#define NEW_ROUTE_VIA_ARGS 5

/**
 * New route command specified number
 */
#define NEW_ROUTE 5

/**
 * Number of args of new route command
 */
#define NEW_ROUTE_ARGS 3

/**
 * Extend route command specified number
 */
#define EXTEND_ROUTE 6

/**
 * Number of args of new route command
 */
#define EXTEND_ROUTE_ARGS 2

/**
 * Remove road command specified number
 */
#define REMOVE_ROAD 7

/**
 * Number of args of remove road command
 */
#define REMOVE_ROAD_ARGS 2

/**
 * Remove route command specified number
 */
#define REMOVE_ROUTE 8

/**
 * Number of args of remove route command
 */
#define REMOVE_ROUTE_ARGS 1


/**
 * Type for command structure
 */
typedef struct command command_t;

/**
 * Type for union of different arguments of command
 */
typedef union command_argument arg_t;

/**
 * Union of possible types of aruments of commands used in program
 */
union command_argument{
    unsigned id;                ///< Route id number
    const char *city;           ///< City name string
    int year;                   ///< Year of build/repair road
    unsigned length;            ///< Length of road
    const char **cities;        ///< Array of cities names strings
    int *years;                 ///< Array of years of build of roads
    unsigned *lengths;          ///< Array of lengths of roads
    unsigned numberOfCities;    ///< number of cities in new created route
};

/**
 * Structure to represent the possible input command which has its
 * type and dynamically allocated array of arguments (which can have
 * specific types)
 */
struct command {
    unsigned commandType;           ///< Command type number
    union command_argument *args;   ///< Array of arguments of command
};

#endif //IPP_ROADS_INPUT_COMMAND_H
