/** @file
 * Implementation of the class executing the commands based on parser decisions.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#include "commands_manager.h"

bool executeCommand(Map *map, command_t cmd, bool hasCommand, bool inputError, const char **outString)
{
    bool success = false;
    *outString = NULL;
    if (hasCommand && !inputError) {
        switch (cmd.commandType)
        {
            case ADD_ROAD: {
                success = addRoad(map, cmd.args[0].city, cmd.args[1].city,
                                       cmd.args[2].length, cmd.args[3].year);
            } break;
            case REPAIR_ROAD: {
                success = repairRoad(map, cmd.args[0].city, cmd.args[1].city, cmd.args[2].year);
            } break;
            case GET_ROUTE_DESCRIPTION: {
                *outString = getRouteDescription(map, cmd.args[0].id);
                success = (*outString != NULL);
            } break;
            case NEW_ROUTE: {
                success = newRoute(map, cmd.args[0].id, cmd.args[1].city, cmd.args[2].city);
            } break;
            case EXTEND_ROUTE: {
                success = extendRoute(map, cmd.args[0].id, cmd.args[1].city);
            } break;
            case REMOVE_ROAD: {
                success = removeRoad(map, cmd.args[0].city, cmd.args[1].city);
            } break;
            case REMOVE_ROUTE: {
                success = removeRoute(map, cmd.args[0].id);
            } break;
            case NEW_ROUTE_VIA: {
                success = newRouteVia(map, cmd.args[0].id, cmd.args[1].cities,
                                      cmd.args[2].years, cmd.args[3].lengths,
                                      cmd.args[4].numberOfCities);
                mem_free(cmd.args[1].cities);
                mem_free(cmd.args[2].years);
                mem_free(cmd.args[3].lengths);
            } break;
            default: {
                success = false;
            }
        }
        mem_free(cmd.args);
        return success;
    }
    else return !hasCommand && !inputError;
}

void printError(uint64_t lineNumber)
{
    fprintf(stderr, "ERROR %" PRIu64 "\n", lineNumber);
}

void manageOutData(const char *data)
{
    if (data != NULL) fprintf(stdout, "%s\n", data);
    mem_free((void *) data);
}