#ifndef IPP_ROADS_MEMORY_TESTING_H
#define IPP_ROADS_MEMORY_TESTING_H

#include <stdlib.h>
#include <stdio.h>

void* __real_malloc(size_t size);

void* __real_realloc(void *oldMem, size_t newSize);

void* __wrap_malloc(size_t c)
{
    printf ("malloc called with %zu\n", c);
    return __real_malloc (c);
}

void* __wrap_realloc(void *oldMem, size_t newSize)
{
    printf ("realloc called with %zu\n", newSize);
    return __real_realloc(oldMem, newSize);
}

#endif //IPP_ROADS_MEMORY_TESTING_H
