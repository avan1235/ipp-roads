/** @file
 * Interface of the class parsing the data from string data line.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_PARSER_H
#define IPP_ROADS_PARSER_H

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "memory_manager.h"
#include "input_command.h"
#include "input_properties.h"

/**
 * Add road command text
 */
#define ADD_ROAD_COMMAND "addRoad"

/**
 * Repair road command text
 */
#define REPAIR_ROAD_COMMAND "repairRoad"

/**
 * Get route description command text
 */
#define GET_ROUTE_DESCRIPTION_COMMAND "getRouteDescription"

/**
 * New route command text
 */
#define NEW_ROUTE_COMMAND "newRoute"

/**
 * Extend route command text
 */
#define EXTEND_ROUTE_COMMAND "extendRoute"

/**
 * Remove road command text
 */
#define REMOVE_ROAD_COMMAND "removeRoad"

/**
 * Remove route command text
 */
#define REMOVE_ROUTE_COMMAND "removeRoute"

/**
 * Minus symbol for strings numbers
 */
#define MINUS '-'

/**
 * String representation of max int value
 */
#define MAX_INT_STRING "2147483647"

/**
 * String representation of min int value
 */
#define MIN_INT_STRING "-2147483648"

/**
 * String representation of amx uint value
 */
#define MAX_UINT_STRING "4294967295"

/**
 * @brief Parse the input string line data
 * Parse the line into command structure, allocate memory for arguments of
 * command (which has to be freed manually) and set the arguments to proper
 * values got from input line
 * @param[in] line              – input line string to be parsed
 * @param[in,out] command       – pointer to command to be set
 * @param[in] hasCommand        – pointer to bool value which describe command
 * @param[in] inputError        – pointer to bool value which describe errors
 * @return                      – true when parsed command, false on memory
 *                                allocation error
 */
bool parseData(char *line, command_t *command, bool *hasCommand, bool *inputError);

#endif //IPP_ROADS_PARSER_H
