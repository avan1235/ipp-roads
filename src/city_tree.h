/** @file
 * Interface of the class keeping all cities in ternary
 * tree data structure.
 *
 * @author Maciej Procyk <m.procyk@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 29.04.2019
 */

#ifndef IPP_ROADS_CITY_TREE_H
#define IPP_ROADS_CITY_TREE_H

#include <stdlib.h>
#include <stdbool.h>

#include "memory_manager.h"
#include "vector.h"

/**
 * End of string char
 */
#define END_OF_STRING '\0'

/**
 * Ternary tree data structure for keeping the
 * all names of cities an easily reconstruct the
 * strings having the pointer to the last node
 * in city name and the length of the city name
 */

/**
 * Type of tree node structure.
 */
typedef struct tree_node tree_node_t;

/**
 * Pointer defined type to simplify the usage of
 * trees and call the tree as 'tree_t' not as the
 * node pointer 'tree_node_t*'
 */
typedef tree_node_t* tree_t;

/**
 * Union for representing the tree node son as
 * usually it is the next tree but for leafs
 * the son is a graph node number connected with
 * inserted city name
 */
union tree_child {
    tree_t son;                     ///< Next tree son
    unsigned graph_node_number;     ///< Graph vertex number for last tree
};

/**
 * Structure to represent the ternary tree node
 * with the parent pointer to be able to recover the inserted
 * cities names from the pointers to leafs of tree.
 * Keeps one split char and the pointers to three sons.
 */
struct tree_node {
    char splitChar;         ///< Char in current tree node
    tree_t parent;          ///< Prent tree of node
    tree_t lower;           ///< Tree with lower char
    union tree_child next;  ///< Tree with the same char or graph node number
    tree_t higher;          ///< Tree with higher char
};

/**
 * @brief Insert new city to tree.
 * Insert new city name given as the standard C string and save
 * it connected with the specified graph node number.
 * When encounter problem in memory allocation stops allocating
 * new nodes and gives the user to free all the nodes by saving the
 * pointers to them in user allocated backup memory and gives
 * the number of allocated nodes to get the user the size
 * of array to be freed.
 * When finish creating nodes successfully sets leafPtr to the last
 * inserted tree node to get the ability of recovering cities
 * names from the bottom of the tree.
 * @param[in,out] treePtr                 – pointer to tree which is the new
 *                                          allocated nodes main root
 * @param[in] city                        – string of city name
 * @param[in] nodeNumber                  – number of node in graph which will
 *                                          be connected with inserted city
 * @param[in,out] leafPtr                 – pointer to tree which will be after
 *                                          insertion the leaf
 * @param[in,out] newNodes                – array of backup memory
 * @param[in,out] numberOfInsertedNodes   – number of inserted nodes pointer
 * @return                                – true when successfully allocated all
 *                                          tree nodes, otherwise false
 */
bool insertToTree(tree_t *treePtr, const char *city, unsigned nodeNumber,
                  tree_t *leafPtr, tree_t *newNodes, unsigned *numberOfInsertedNodes);

/**
 * @brief Search for given city in tree and get its number.
 * Search in tree from the top of the tree to the bottom for given
 * city name to be able to get the node in graph number connected with
 * the given city name
 * @param[in] tree                      – tree to search the city in
 * @param[in] city                      – city name to search in tree
 * @param[in,out] nodeNumber            – pointer to place where node number
 *                                        will be saved
 * @return                              – true when found city in tree
 *                                        otherwise false
 */
bool searchInTree(tree_t tree, const char *city, unsigned *nodeNumber);

/**
 * @brief Get the name of the city in tree.
 * Recover string name of specified city by giving the pointer to
 * the last character of city name in tree and the length of the name.
 * Memory for the string has to be allocated outside the recover function
 * @param[in] leaf                  – leaf of tree to start searching city name
 * @param[in] len                   – length of city name
 * @param[in,out] stringPtr         – pointer to allocated memory for string
 */
void recoverNameFromTree(tree_t leaf, size_t len, char *stringPtr);

/**
 * @brief Free memory allocated for data structure.
 * Free all memory allocated for tree nodes in recursive way.
 * @param[in] tree      – tree to be freed
 */
void freeTree(tree_t tree);

#endif //IPP_ROADS_CITY_TREE_H
